package be.uantwerpen.mosaic.greenhouse;

import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import com.sun.spot.peripheral.radio.LowPan;


/**
 * The ScheduleSink class defines the intervals for the immediate children nodes to send their data.
 * It also checks that in each turn the nodes conform to the schedule
 */
public class ScheduleSink {
	public final long     ADDRESS_PREFIX        = 0x00144F0100000000l;
	private Timer timerSender;
	
	private GenericSender sender;
	private final Vector children;
	long[] child_schedule = new long[2];
	boolean[] child_acks = new boolean[2];

	private long startingTime;

	/**
	 * 
	 */
	public ScheduleSink(GenericSender init_sender) {
		timerSender = new Timer();
		sender = init_sender;
		children = ((MyRoutingMgr) (LowPan.getInstance().getRoutingManager())).myChildren;

		defineSchedule();
	}
	
	public long getAbsoluteTime(){
		return ((ReceiverApp)sender).getAbsoluteTime();
	}

	public void defineSchedule() {
		if (children.size()==0){

		}else{
			startingTime = System.currentTimeMillis();
			for (int i = 0; i < children.size(); i++) {	
				System.out.println(getAbsoluteTime() + " - Schedule for "+((Address)children.elementAt(i)).getID());
				child_schedule[i] = (i * 15000) / (children.size() + 1);
				child_acks[i] = false;
				SendSchedule(((Address) children.elementAt(i)).address, child_schedule[i]);
			}
			scheduleCheckSch();
		}
	}

	public int indexOfChild(long childAddress){
		int index = -1;
		for (int i=0; i<children.size(); i++) {	
			if (((Address) children.elementAt(i)).address==childAddress){
				index = i;
			}
		}
		return index;
	}
	
	public void receivedAckBy(long address){
		int index = indexOfChild(address);
		if(index>=0){
			child_acks[index] = true;
		}
	}

	public boolean checkSchedule(){
		boolean ok = true;
		if (children.size()>0){
			int k = 0;
			for (int i = 0; i < children.size(); i++) {	
				if (! child_acks[i]){
					long middleTime = System.currentTimeMillis();
					long additionalTime = (30*1000) - (middleTime - startingTime) % (30*1000);
					child_schedule[i] = additionalTime + (i * 15000) / (children.size() + 1);
					SendSchedule(((Address) children.elementAt(i)).address, child_schedule[i]);
					k++;
					ok = false;
				}
			}
		}
		return ok;
	}

	public void SendSchedule(long child, long child_schedule) {
		final ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
		final DataOutput            data_output   = new DataOutputStream(output_stream);

		short type = 1;
		short address = (short) (child - this.ADDRESS_PREFIX);

		try {
			data_output.writeShort(type);
			data_output.writeShort(address);
			data_output.writeLong(child_schedule);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final byte[] data = output_stream.toByteArray();
		final int length = output_stream.size();
		sender.sendSchedule(data, length, new Address(child));

		System.out.println(getAbsoluteTime() + " - Schedule sent " + 
				Integer.toHexString(address) + ": " + child_schedule);
	}
	
	private class CheckSchedule extends TimerTask {

		public CheckSchedule() {
			super();
		}

		public void run() {
			if (! checkSchedule()){
				scheduleCheckSch();
			} else {
				long time = System.currentTimeMillis();
				// next check in the next frame (30s) + after 30s (tot 60s) - position in current frame
				scheduleCheckSch(60000l - ((time - startingTime) % 30000l));
			}
		}

	}
	
	private void scheduleCheckSch() {
		timerSender.schedule(new CheckSchedule(), 5000l);
	}
	
	private void scheduleCheckSch(long interval) {
		for (int i = 0; i < children.size(); i++) {	
			child_acks[i] = false;
		}
		timerSender.schedule(new CheckSchedule(), interval);
	}
}
