package be.uantwerpen.mosaic.greenhouse;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;

import be.uantwerpen.mosaic.greenhouse.MyRoutingMgr;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.radio.ILowPan;
import com.sun.spot.peripheral.radio.LowPan;
import com.sun.spot.peripheral.radio.RadioPolicy;
import com.sun.spot.peripheral.radio.routing.interfaces.IRoutingManager;
import com.sun.spot.service.BootloaderListenerService;

/**
 * MIDLet for the Greenhouse Sink application
 * 
 * @author Bart Sas
 * 
 */
public class ReceiverApp extends MIDlet implements Runnable, GenericSender {

	private static final int LISTEN_PORT = 10;
	private static final int DESTINATION_PORT = 10;

	private Thread thread;
	private volatile boolean running;
	private RadiogramConnection connection;
	private GreenhouseSink sink;
	private AggregatorSink aggregator;
	private ScheduleSink scheduler;
	
	private long initialTime;

	/**
	 * 
	 */
	public ReceiverApp() {
		thread = new Thread(this);
		running = false;
		connection = null;
		sink = null;
		initialTime = System.currentTimeMillis();
	}
	
	public long getAbsoluteTime(){
		return System.currentTimeMillis() - initialTime;
	}

	public void startApp() {
		ILowPan lowPan = LowPan.getInstance();
		IRoutingManager newRM = new MyRoutingMgr();
		lowPan.setRoutingManager(newRM);

		BootloaderListenerService.getInstance().start();
		Spot.getInstance().getRadioPolicyManager().setRxOn(true);

		try {
			connection = (RadiogramConnection) Connector.open("radiogram://:"
					+ String.valueOf(LISTEN_PORT));
			connection.setRadioPolicy(RadioPolicy.AUTOMATIC);

			sink = new GreenhouseSink();
			scheduler = new ScheduleSink(this);
			aggregator = new AggregatorSink(sink,scheduler);
			
			running = true;

			thread.start();
		} catch (final IOException exception) {
			exception.printStackTrace();
			running = false;
		}
	}

	public void pauseApp() {
		// Ignore this event.
	}

	public void destroyApp(final boolean unconditional) {
		if (running) {
			running = false;

			try {
				connection.close();
				thread.join();
				sink.stop();
			} catch (final Exception exception) {
				// Ignore any exceptions.
			}
		}
	}

	public void run() {
		try {
			final Datagram datagram = connection.newDatagram(connection
					.getMaximumLength());

			while (running) {
				try {
					datagram.reset();
					connection.receive(datagram);

					final byte[] data = datagram.getData();
					final int offset = datagram.getOffset();
					final int length = datagram.getLength();

					dataReceived(data, offset, length);
				} catch (final IOException exception) {
					exception.printStackTrace();
				}
			}
		} catch (final IOException exception) {
			exception.printStackTrace();
		}
	}

	public void dataReceived(byte[] data, int offset, int length){
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		DataInput   data_input   = new DataInputStream(input);
		try {
			short type = data_input.readShort();
			long address = scheduler.ADDRESS_PREFIX + data_input.readShort();
			if (type==0){ // data packets
				scheduler.receivedAckBy(address);
				System.out.println(getAbsoluteTime() + " - Received packets - " + aggregator.dataReceived(data_input));
			}else if (type==2){ // schedule confirmation
				scheduler.receivedAckBy(address);
				System.out.println(getAbsoluteTime() + " - Ack received from " +
						Integer.toHexString((int)address));
			}else{
				System.err.println(getAbsoluteTime() + " - Unknown packet type");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendData(byte[] data, int length) {
		// TODO Auto-generated method stub

	}

	public void sendSchedule(byte[] data, int length, Address address) {
		try
		{
			RadiogramConnection connection_child = (RadiogramConnection) Connector.open("radiogram://" 
					+ address.toString() + ":" + String.valueOf(DESTINATION_PORT));
			connection_child.setRadioPolicy(RadioPolicy.AUTOMATIC);
			final Datagram datagram = connection_child.newDatagram(length);
			datagram.write(data, 0, length);
			try{
				connection_child.send(datagram);
			}catch(final Exception ex){
				System.out.println(ex.toString());
			}
			connection_child.close();
		}
		catch (final IOException exception)
		{
			exception.printStackTrace();
		}
	}

}
