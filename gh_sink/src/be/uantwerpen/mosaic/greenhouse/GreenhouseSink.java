package be.uantwerpen.mosaic.greenhouse;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.util.IEEEAddress;
import com.sun.squawk.util.IntHashtable;



/**
 * The GreenhouseSink class processes the messages received from the different Monitor nodes.
 * Depending on if messages arive on time or too late the appropriate led will be colored either red or green
 * @author Bart Sas
 */
public class GreenhouseSink {

        private static final long     ADDRESS_PREFIX        = 0x00144F0100000000l;

        private static final LEDColor GREEN                 = new LEDColor(0, 100, 0);
        private static final LEDColor RED                   = new LEDColor(100, 0, 0);
        private static final LEDColor BLUE                  = new LEDColor(0, 0, 100);
        private static final String   MAXIMUM_DELAY_KEY     = "greenhouse.max_delay";

        private static final long     DEFAULT_MAXIMUM_DELAY = 90; // seconds
        private static final LEDColor TOO_LATE_COLOR        = RED;
        private static final LEDColor ON_TIME_COLOR         = GREEN;
        private static final LEDColor NEW_NODE_COLOR        = BLUE;


        private ITriColorLED[] leds;
        private long           maximum_delay;
        private Timer          expiry_timer;
        private IntHashtable   monitors;

        private class Monitor {

                private class MessageTooLate extends TimerTask {

                        public MessageTooLate() {
                                expiry_timer.schedule(this, maximum_delay);
                        }

                        public void run() {
                                synchronized(Monitor.this) {
                                        led.setColor(TOO_LATE_COLOR);
                                        led.setOn();
                                }
                        }

                }

                private ITriColorLED   led;
                private long           last_update;
                private MessageTooLate message_too_late;

                public Monitor() {
                        synchronized(leds) {
                                final int led_ix = monitors.size();

                                if(led_ix < leds.length) {
                                        led = leds[led_ix];
                                        led.setColor(NEW_NODE_COLOR);
                                        led.setOn();
                                }
                                else {
                                        led = null;
                                }
                        }

                        last_update      = System.currentTimeMillis();
                        message_too_late = new MessageTooLate();
                }

                public synchronized void dataReceived() {
                        final long current_time = System.currentTimeMillis();
                        final long delay        = current_time - last_update;

                        if(delay <= maximum_delay && led != null) {
                                led.setColor(ON_TIME_COLOR);
                                led.setOn();
                        }

                        message_too_late.cancel();
                        message_too_late = new MessageTooLate();
                        last_update = current_time;
                }
        }

        /**
         * 
         */
        public GreenhouseSink() {
                leds          = EDemoBoard.getInstance().getLEDs();
                maximum_delay = getLongParameter(MAXIMUM_DELAY_KEY, DEFAULT_MAXIMUM_DELAY) * 1000l;
                expiry_timer  = new Timer();
                monitors      = new IntHashtable();

                // Turn all the LEDs off.
                for(int i = 0; i < leds.length; ++i) {
                        leds[i].setOff();
                }
        }

        private static long getLongParameter(final String key, final long default_value) {
                final String value = System.getProperty(key);

                if(value != null) {
                        try {
                                return Long.parseLong(value);
                        }
                        catch(final NumberFormatException exception) {
                                System.err.println(
                                        "Warning: could not parse value of "
                                                + key
                                                + " ("
                                                + value
                                                + "), using default value ("
                                                + String.valueOf(default_value)
                                                + ")"
                                );
                        }
                }
                else {
                        System.err.println(
                                "No value provided for "
                                        + key
                                        + ", using default value ("
                                        + String.valueOf(default_value)
                                        + ")"
                        );
                }

                return default_value;
        }

        /**
         * Signal that new data has been received
         * @param data
         * @param offset
         * @param length
         */
        public void dataReceived(final byte[] data, final int offset, final int length) {
                final InputStream input_stream = new ByteArrayInputStream(data, offset, length);
                final DataInput   data_input   = new DataInputStream(input_stream);

                try {
                        final short address = data_input.readShort();
                        System.out.println("Sink received update from node: " + IEEEAddress.toDottedHex(ADDRESS_PREFIX | address));

                        synchronized(monitors) {
                                final Monitor monitor = (Monitor)monitors.get(address);

                                if(monitor == null) {
                                        // Add the monitor to the list of monitors do not indicate its status yet until
                                        // either a second message arrives or its timer expires.
                                        monitors.put(address, new Monitor());
                                }
                                else {
                                        monitor.dataReceived();
                                }
                        }
                }
                catch(final IOException exception) {
                        // If messages contain invalid data they are just ignored and their timer will
                        // expire.
                }
        }

        /**
         * Stop the Greenhouse Sink
         */
        public void stop() {
                expiry_timer.cancel();
        }

}
