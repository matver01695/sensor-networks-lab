package be.uantwerpen.mosaic.greenhouse;

/**
 * 
 * @author Bart Sas
 *
 */
public interface DataSender {

        /**
         * Send a chunk of data to the sink in the network
         * @param data - the byte array containing the data to be sent
         * @param length - the number of bytes in the byte-array that should be sent
         */
        public void sendData(final byte[] data, final int length);
}
