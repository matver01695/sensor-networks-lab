package be.uantwerpen.mosaic.greenhouse;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

public class Packet {
	public Address address;
	public int light;
	public double temp;

	public Packet(DataInput data_input){
		try {
			address = new Address(data_input.readShort());
			light = data_input.readInt();
			temp = data_input.readDouble();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Packet(short addr, int light, double temp){
		this.address = new Address(addr);
		this.light = light;
		this.temp = temp;
	}

	public static Vector parseMultiplePackets(DataInput data_input){
		Vector pckts = new Vector();
		while (true){
			try {
				pckts.addElement(new Packet(data_input.readShort(), data_input.readInt(), data_input.readDouble()));
			} catch (IOException e) {
				//System.out.println(System.currentTimeMillis() + "Sys - Lost something: " 
				//	+ e.getMessage() );
				break;
			}
		}
		return pckts;
	}

	public static ByteArrayOutputStream packetsToByte(Vector pckts){
		final ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
		final DataOutput            data_output   = new DataOutputStream(output_stream);

		try {
			data_output.writeShort(0); // packet type
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for (int i=0; i<pckts.size(); i++){
			Packet p = (Packet) pckts.elementAt(i);
			try {
				data_output.writeShort(p.address.id);
				data_output.writeInt(p.light);
				data_output.writeDouble(p.temp);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error on packetsToByte()");
			}
		}

		return output_stream;
	}
	
	
	public ByteArrayOutputStream toByte() {
		final ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
		final DataOutput            data_output   = new DataOutputStream(output_stream);

		try {
			data_output.writeShort(address.id);
			data_output.writeInt(light);
			data_output.writeDouble(temp);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error on toByte()");
		}


		return output_stream;
	}
}