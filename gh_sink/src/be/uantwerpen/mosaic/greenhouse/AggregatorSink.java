package be.uantwerpen.mosaic.greenhouse;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.util.Vector;

public class AggregatorSink {

	public static final long     ADDRESS_PREFIX        = 0x00144F0100000000l;

	private final GreenhouseSink sink;
	private final ScheduleSink scheduler;
	
	public AggregatorSink(GreenhouseSink sink, ScheduleSink scheduler) {
		this.sink = sink;
		this.scheduler = scheduler;
		
	}

	/**
	 * Signal that new data has been received
	 * @param data
	 * @param offset
	 * @param length
	 */
	public int dataReceived(final DataInput   data_input) {
		int i = 0;
		try {
			String packetsReceived = " - Packets";
			Vector pckts = Packet.parseMultiplePackets(data_input);
			for (i=0; i<pckts.size(); i++){
				Packet p = (Packet)pckts.elementAt(i);
				final short address = p.address.id; //data_input.readShort();
				//System.out.println("Sink received update from node: " + IEEEAddress.toDottedHex(ADDRESS_PREFIX | address));
				//System.out.println("Light:"+p.light+" - Temperature:"+p.temp);
				packetsReceived += " - " + p.address.id;
				
				final ByteArrayOutputStream output_stream = p.toByte();

				final byte[] data = output_stream.toByteArray();
				final int length = output_stream.size();

				scheduler.receivedAckBy(ADDRESS_PREFIX | address);
				sink.dataReceived(data, 0, length);
			}
			System.out.println(scheduler.getAbsoluteTime() + packetsReceived);
			return i;
		}
		catch(final Exception exception) {
			// If messages contain invalid data they are just ignored and their timer will
			// expire.
			System.out.println(scheduler.getAbsoluteTime() + " - Lost something: " 
						+ exception.getMessage() );
			return i;
		}
	}
}
