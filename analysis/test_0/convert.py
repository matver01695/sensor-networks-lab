spots = ['5812','1193','1A57','1579','16D7','12B5','1154','1519']
i = 1

for spot in spots:
	f = open(spot + '-trace.txt', 'r')
	fRes = open(spot + '-dc.txt', 'w')
	start = False
	scheduleReset = 0
	initialAbsoluteTime = 0

	for line in f:
		if not start:
			if line == '-run-spotclient-once:\n':
				start = True
		else:
			newLine = str(i) + ';' + spot + ';'
			if line[:12] == '     [java] ':
				l = line[12:]
				words = l.split(' - ')
				if len(words)>=2:
					if words[1] == 'Received schedule from parent\n':
						scheduleReset = long(words[0][:-1]) # reset absolute starting value for new schedule
						if initialAbsoluteTime == 0:
							initialAbsoluteTime = long(words[0][:-1])

					if words[1][:6] == 'ONt = ': # save Duty Cycle values
						time = str((scheduleReset - initialAbsoluteTime + long(words[0]))/(1000.0 * 60.0))
						dc = str(float(words[3][5:]))

						newLine += time + ';'
						newLine += dc + ';\n'

						fRes.write(newLine)

	f.close()
	i += 1
	fRes.close()