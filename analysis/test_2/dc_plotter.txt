# '5812','1193','1A57','1579','16D7','12B5','1154','1519'

set datafile separator ";"
set terminal pdf
set output 'test_2-dc.pdf'

set yrange [0.1:0.8]
set xrange [0:40]

set xlabel "Minutes"
set ylabel "Duty Cycle"

set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"

plot '5812-dc.txt' u 3:4 w l lt 1 title "1 - 5812", \
	'1193-dc.txt' u 3:4 w l lt 2 title "2 - 1193", \
	'1A57-dc.txt' u 3:4 w l lt 3 title "3 - 1A57", \
	'1579-dc.txt' u 3:4 w l lt 4 title "4 - 1579", \
	'16D7-dc.txt' u 3:4 w l lt 5 title "5 - 16D7", \
	'12B5-dc.txt' u 3:4 w l lt 6 title "6 - 12B5", \
	'1154-dc.txt' u 3:4 w l lt 7 title "7 - 1154", \
	'1519-dc.txt' u 3:4 w l lt 8 title "8 - 1519"

unset output