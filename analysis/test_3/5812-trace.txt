Buildfile: /home/students/s0111542/workspace/gh_mon/build.xml

-pre-init:

-do-init:

-post-init:

-warn-jar-file:

init:

-set-selector-for-nonbasestation:

-override-warning-find-spots:
     [echo] 
     [echo] WARNING: Using the port specified in the 
     [echo] "port" property without any checks.
     [echo] 
     [echo] We recommend setting the "spotport" (rather
     [echo] than the "port") property to enable features
     [echo] like interactive selection of another valid 
     [echo] port if the specified port is unavailable.
     [echo]     

-prepare-conditions-for-find-spots:

-find-shared-basestation:

-run-spotfinder:
     [exec] WARNING: Your Linux installation does not
     [exec] include the 'hal-device' application. The
     [exec] spotfinder utility will treat all devices
     [exec] matching the pattern:
     [exec]      /dev/ttyACM*
     [exec] as Sun SPOT devices. If no Sun SPOT device
     [exec] is connected but another device matching this
     [exec] pattern is available, that device will be 
     [exec] selected automatically.
     [exec] 
     [exec] 

-check-spotfinder-result:

-decide-whether-to-run-spotselector:

-run-spotselector:

-collect-spotselector-result:

-clean-up-spotselector-output-file:

-spotselector-fail:

-decide-whether-to-start-basestation-manager:

-start-new-basestation-manager:

-start-shared-basestation:

-do-find-spots:

-jar-and-deploy:

-pre-init:

-do-init:

-post-init:

-warn-jar-file:

init:

-check-for-manifest:

-set-to-jar-name:
     [echo] No to.jar.file specified.
     [echo] Using "suite/Greenhouse Monitor_1.0.0.jar"

-pre-clean:

-do-clean:
   [delete] Deleting directory /home/students/s0111542/workspace/gh_mon/build
   [delete] Deleting directory /home/students/s0111542/workspace/gh_mon/suite
   [delete] Deleting directory /home/students/s0111542/workspace/gh_mon/j2meclasses

-post-clean:

clean:

-pre-compile:

-do-compile:
    [mkdir] Created dir: /home/students/s0111542/workspace/gh_mon/build
    [javac] Compiling 10 source files to /home/students/s0111542/workspace/gh_mon/build
    [javac] Note: /home/students/s0111542/workspace/gh_mon/src/be/uantwerpen/mosaic/greenhouse/MyRoutingMgr.java uses or overrides a deprecated API.
    [javac] Note: Recompile with -Xlint:deprecation for details.

-post-compile:

compile:

-pre-preverify:

-make-preverify-directory:
    [mkdir] Created dir: /home/students/s0111542/workspace/gh_mon/j2meclasses

-unjar-utility-jars:

-do-preverify:

-post-preverify:

preverify:

-pre-jar-app:

-do-jar-app:
    [mkdir] Created dir: /home/students/s0111542/workspace/gh_mon/suite
      [jar] Building jar: /home/students/s0111542/workspace/gh_mon/suite/Greenhouse Monitor_1.0.0.jar

-post-jar-app:

jar-app:

-set-from-jar-name:
     [echo] Using "suite/Greenhouse Monitor_1.0.0.jar"

-check-for-jar:

-pre-suite:

-do-suite:
     [java] CompilerOracle: exclude com/sun/squawk/Method.getParameterTypes
     [java] CompilerOracle: exclude com/sun/squawk/SymbolParser.getSignatureTypeAt
     [java] CompilerOracle: exclude com/sun/squawk/SymbolParser.stripMethods
     [java] [translating suite image [closed: false, parent: transducerlib] ...]
     [java] ### Excluding compile: com.sun.squawk.SymbolParser::getSignatureTypeAt
     [java] ### Excluding compile: com.sun.squawk.Method::getParameterTypes
     [java] [Including resource: META-INF/MANIFEST.MF]
     [java] Romizer processed 22 classes and generated these files:
     [java]   /home/students/s0111542/workspace/gh_mon/image.sym
     [java]   /home/students/s0111542/workspace/gh_mon/image.suite
     [java]   /home/students/s0111542/workspace/gh_mon/image.suite.metadata
     [java]   /home/students/s0111542/workspace/gh_mon/image.suite.api
    [unzip] Expanding: /home/students/s0111542/workspace/gh_mon/suite/Greenhouse Monitor_1.0.0.jar into /home/students/s0111542/workspace/gh_mon/suite
     [move] Moving 1 file to /home/students/s0111542/workspace/gh_mon/suite
     [move] Moving 1 file to /home/students/s0111542/workspace/gh_mon/suite
     [move] Moving 1 file to /home/students/s0111542/workspace/gh_mon/suite
   [delete] Deleting: /home/students/s0111542/workspace/gh_mon/image.suite.api

-post-suite:

-override-warning-find-spots:

-prepare-conditions-for-find-spots:

-find-shared-basestation:

-run-spotfinder:
     [exec] WARNING: Your Linux installation does not
     [exec] include the 'hal-device' application. The
     [exec] spotfinder utility will treat all devices
     [exec] matching the pattern:
     [exec]      /dev/ttyACM*
     [exec] as Sun SPOT devices. If no Sun SPOT device
     [exec] is connected but another device matching this
     [exec] pattern is available, that device will be 
     [exec] selected automatically.
     [exec] 
     [exec] 

-check-spotfinder-result:

-decide-whether-to-run-spotselector:

-run-spotselector:

-collect-spotselector-result:

-clean-up-spotselector-output-file:

-spotselector-fail:

-decide-whether-to-start-basestation-manager:

-start-new-basestation-manager:

-start-shared-basestation:

-do-find-spots:

-pre-deploy:

-set-uri:

-do-deploy:

-check-run-spotclient-parameters:

-run-spotclient-once-with-remote-id:

-run-spotclient-multiple-times-with-remote-id:

-run-spotclient-once-locally:

-echo-progress-for-remote-runs:

-echo-progress-for-local-runs:

-run-spotclient-once:
     [java] SPOT Client starting...
     [java] [waiting for reset] Exiting - detected bootloader command
     [java] [waiting for reset] java.io.InterruptedIOException: Connection was closed
     [java] [waiting for reset]     at java.lang.Throwable.<init>(bci=16)
     [java] [waiting for reset]     at java.lang.Throwable.<init>(bci=5)
     [java] [waiting for reset]     at java.lang.Exception.<init>(bci=6)
     [java] [waiting for reset]     at java.io.IOException.<init>(bci=6)
     [java] [waiting for reset]     at java.io.InterruptedIOException.<init>(bci=6)
     [java] [waiting for reset]     at com.sun.spot.io.j2me.radiogram.Radiogram.receive(bci=55)
     [java] [waiting for reset]     at com.sun.spot.io.j2me.radiogram.RadiogramConnImpl.receive(bci=32)
     [java] [waiting for reset]     at be.uantwerpen.mosaic.greenhouse.MonitorApp$ReceiverComponent.run(MonitorApp.java:326)
     [java] [waiting for reset]     at java.lang.Thread.run(bci=17)
     [java] [waiting for reset]     in virtual method #47 of com.sun.squawk.VMThread(bci=42)
     [java] [waiting for reset]     in static method #3 of com.sun.squawk.VM(bci=6)
     [java] [waiting for reset] 
     [java] [waiting for reset] 
     [java] [waiting for reset] ** VM stopped: exit code = 0 ** 
     [java] [waiting for reset] 
     [java] 
     [java] Sun SPOT bootloader (yellow-mosaic-140916-1) 
     [java] SPOT serial number = 0014.4F01.0000.5812
     [java] Writing SPOT properties (494 bytes) to local SPOT on port /dev/ttyACM3
     [java] |============================================================| 100%
     [java] 
     [java] 
     [java] Using target file name: spotsuite://Sun_Microsystems_Inc/Greenhouse_Monitor
     [java] Relocating application suite to 0x10900000
     [java] About to flash from /home/students/s0111542/workspace/gh_mon/suite/image
     [java] Writing imageapp512302942038743185.bintemp (31109 bytes) to local SPOT on port /dev/ttyACM3
     [java] |===                                                         | 6%
     [java] |======                                                      | 11%
     [java] |=========                                                   | 16%
     [java] |============                                                | 21%
     [java] |===============                                             | 26%
     [java] |==================                                          | 31%
     [java] |=====================                                       | 36%
     [java] |========================                                    | 41%
     [java] |===========================                                 | 45%
     [java] |==============================                              | 50%
     [java] |=================================                           | 55%
     [java] |====================================                        | 60%
     [java] |=======================================                     | 65%
     [java] |==========================================                  | 70%
     [java] |=============================================               | 75%
     [java] |================================================            | 80%
     [java] |===================================================         | 85%
     [java] |======================================================      | 90%
     [java] |=========================================================   | 95%
     [java] |============================================================| 100%
     [java] 
     [java] 
     [java] Using suite name: spotsuite://Sun_Microsystems_Inc/Greenhouse_Monitor and Midlet number 1
     [java] 
     [java] Exiting
     [java] RXTX fhs_lock() Error: creating lock file: /var/lock/LCK..ttyACM2: File existsRXTX fhs_lock() Error: creating lock file: /var/lock/LCK..ttyACM1: File existsRXTX fhs_lock() Error: creating lock file: /var/lock/LCK..ttyACM0: File existsRXTX fhs_lock() Error: creating lock file: /var/lock/LCK..ttyACM2: File existsRXTX fhs_lock() Error: creating lock file: /var/lock/LCK..ttyACM1: File existsRXTX fhs_lock() Error: creating lock file: /var/lock/LCK..ttyACM0: File exists

-run-spotclient-multiple-times-locally:

-run-spotclient:

-post-deploy:

flashapp:

-do-deploy-only:

-do-jar-and-deploy:

-deploy-only:

deploy:

-pre-init:

-do-init:

-post-init:

-warn-jar-file:

init:

-set-selector-for-nonbasestation:

-override-warning-find-spots:

-prepare-conditions-for-find-spots:

-find-shared-basestation:

-run-spotfinder:
     [exec] WARNING: Your Linux installation does not
     [exec] include the 'hal-device' application. The
     [exec] spotfinder utility will treat all devices
     [exec] matching the pattern:
     [exec]      /dev/ttyACM*
     [exec] as Sun SPOT devices. If no Sun SPOT device
     [exec] is connected but another device matching this
     [exec] pattern is available, that device will be 
     [exec] selected automatically.
     [exec] 
     [exec] 

-check-spotfinder-result:

-decide-whether-to-run-spotselector:

-run-spotselector:

-collect-spotselector-result:

-clean-up-spotselector-output-file:

-spotselector-fail:

-decide-whether-to-start-basestation-manager:

-start-new-basestation-manager:

-start-shared-basestation:

-do-find-spots:

-pre-run:

-do-run:

-check-run-spotclient-parameters:

-run-spotclient-once-with-remote-id:

-run-spotclient-multiple-times-with-remote-id:

-run-spotclient-once-locally:

-echo-progress-for-remote-runs:

-echo-progress-for-local-runs:

-run-spotclient-once:
     [java] SPOT Client starting...
     [java] 
     [java] Local Monitor (yellow-101117-1) 
     [java] SPOT serial number = 0014.4F01.0000.5812
     [java] 
     [java] 
     [java] ** VM stopped: exit code = 0 ** 
     [java] 
     [java] Starting midlet 1 in spotsuite://Sun_Microsystems_Inc/Greenhouse_Monitor
     [java] *** Radio will only handle packets received from: 
     [java] ***    0014.4F01.0000.15BD
     [java] ***    0014.4F01.0000.1A57
     [java] Warning: no value provided for greenhouse.min_interval, using default value (27)
     [java] Warning: no value provided for greenhouse.max_interval, using default value (33)
     [java] 1529A - Started receiver thread
     [java] 46720A - Received schedule from parent
     [java] 3 - Forwarding packets from cache
     [java] 28 - Data sent, cache empty
     [java] 248 - Ack sent
     [java] 46973A - Initial - ONt = 45.99859619140625 - OFFt = 0.001495361328125 - DC = 0.9999674922097402
     [java] 2769 - Schedule for 1a57
     [java] 2814 - Schedule sent 1a57: 500
     [java] 2819 - Turned On
     [java] 49544A - ONt = 3.000152587890625 - OFFt = 0.001495361328125 - DC = 0.9995018198824701
     [java] 3017 - Ack received from 1a57
     [java] 3348 - Ack received from 1a57
     [java] 50076A - Received data to FWD, saved in cache 2 elements [1a57, 1a57, ]
     [java] 7839 - Turned Off
     [java] 7842 - Next on after: 24927
     [java] 54567A - ONt = 8.550201416015625 - OFFt = 0.001495361328125 - DC = 0.9998251386400783
     [java] 30031 - Forwarding packets from cache
     [java] 30065 - Data sent, cache empty
     [java] 32778 - Turned On
     [java] 79503A - ONt = 8.5599365234375 - OFFt = 25.0654296875 - DC = 0.2545678304212242
     [java] 33410 - Ack received from 1a57
     [java] 80141A - Received data to FWD, saved in cache 7 elements [12b5, 12b5, 12b5, 1154, 1154, 1154, 1a57, ]
     [java] 37795 - Turned Off
     [java] 37797 - Next on after: 24972
     [java] 84522A - ONt = 13.604278564453125 - OFFt = 25.0654296875 - DC = 0.3518071167181873
     [java] 60068 - Forwarding packets from cache
     [java] 60129 - Data sent, cache empty
     [java] 62778 - Turned On
     [java] 109503A - ONt = 13.624725341796875 - OFFt = 50.157958984375 - DC = 0.21361166413321142
     [java] 63447 - Ack received from 1a57
     [java] 110178A - Received data to FWD, saved in cache 6 elements [12b5, 1519, 1519, 1519, 1154, 1a57, ]
     [java] 67796 - Turned Off
     [java] 67798 - Next on after: 24971
     [java] 114523A - ONt = 18.668060302734375 - OFFt = 50.157958984375 - DC = 0.27123550796770796
     [java] 90133 - Forwarding packets from cache
     [java] 90194 - Data sent, cache empty
     [java] 92778 - Turned On
     [java] 139503A - ONt = 18.6881103515625 - OFFt = 75.24285888671875 - DC = 0.19895579171716055
     [java] 93485 - Ack received from 1a57
     [java] 140215A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 97795 - Turned Off
     [java] 97797 - Next on after: 24972
     [java] 144522A - ONt = 23.73077392578125 - OFFt = 75.24285888671875 - DC = 0.23976864596591974
     [java] 120198 - Forwarding packets from cache
     [java] 120234 - Data sent, cache empty
     [java] 122778 - Turned On
     [java] 169503A - ONt = 23.74188232421875 - OFFt = 100.34945678710938 - DC = 0.19132586121033637
     [java] 123523 - Ack received from 1a57
     [java] 170253A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 127796 - Turned Off
     [java] 127798 - Next on after: 24971
     [java] 174523A - ONt = 28.783447265625 - OFFt = 100.34945678710938 - DC = 0.2228978545535584
     [java] 150237 - Forwarding packets from cache
     [java] 150273 - Data sent, cache empty
     [java] 152778 - Turned On
     [java] 199503A - ONt = 28.794525146484375 - OFFt = 125.44992065429688 - DC = 0.18668111514157698
     [java] 153561 - Ack received from 1a57
     [java] 200300A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 157796 - Turned Off
     [java] 157798 - Next on after: 24971
     [java] 204523A - ONt = 33.839752197265625 - OFFt = 125.44992065429688 - DC = 0.21244159518614822
     [java] 180277 - Forwarding packets from cache
     [java] 180313 - Data sent, cache empty
     [java] 182779 - Turned On
     [java] 229504A - ONt = 33.850860595703125 - OFFt = 150.5572509765625 - DC = 0.18356492188489057
     [java] 183600 - Ack received from 1a57
     [java] 230330A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 187796 - Turned Off
     [java] 187798 - Next on after: 24971
     [java] 234523A - ONt = 38.893463134765625 - OFFt = 150.5572509765625 - DC = 0.20529594368227302
     [java] 210316 - Forwarding packets from cache
     [java] 210351 - Data sent, cache empty
     [java] 212778 - Turned On
     [java] 259503A - ONt = 38.90447998046875 - OFFt = 175.65869140625 - DC = 0.18131946749775205
     [java] 213638 - Ack received from 1a57
     [java] 260368A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 217795 - Turned Off
     [java] 217797 - Next on after: 24972
     [java] 264522A - ONt = 43.944427490234375 - OFFt = 175.65869140625 - DC = 0.20010839422981383
     [java] 240355 - Forwarding packets from cache
     [java] 240391 - Data sent, cache empty
     [java] 242778 - Turned On
     [java] 289503A - ONt = 43.955596923828125 - OFFt = 200.75094604492188 - DC = 0.1796257525056918
     [java] 243677 - Ack received from 1a57
     [java] 290407A - Received data to FWD, saved in cache 4 elements [12b5, 1a57, 1519, 1154, ]
     [java] 247795 - Turned Off
     [java] 247797 - Next on after: 24972
     [java] 294522A - ONt = 48.997711181640625 - OFFt = 200.75094604492188 - DC = 0.19618808655772577
     [java] 270394 - Forwarding packets from cache
     [java] 270429 - Data sent, cache empty
     [java] 272779 - Turned On
     [java] 319504A - ONt = 49.008758544921875 - OFFt = 225.84432983398438 - DC = 0.17830892435656212
     [java] 273715 - Ack received from 1a57
     [java] 320445A - Received data to FWD, saved in cache 4 elements [1a57, 12b5, 1519, 1154, ]
     [java] 277796 - Turned Off
     [java] 277798 - Next on after: 24971
     [java] 324523A - ONt = 54.0501708984375 - OFFt = 225.84432983398438 - DC = 0.19310908487662382
     [java] 300433 - Forwarding packets from cache
     [java] 300469 - Data sent, cache empty
     [java] 302778 - Turned On
     [java] 349504A - ONt = 54.0625 - OFFt = 250.93984985351562 - DC = 0.17725273272800932
     [java] 303755 - Ack received from 1a57
     [java] 350485A - Received data to FWD, saved in cache 5 elements [1a57, 12b5, 1519, 1154, 1a57, ]
     [java] 307795 - Turned Off
     [java] 307797 - Next on after: 24972
     [java] 354522A - ONt = 59.103668212890625 - OFFt = 250.93984985351562 - DC = 0.19063023339914362
     [java] 330472 - Forwarding packets from cache
     [java] 330508 - Data sent, cache empty
     [java] 332778 - Turned On
     [java] 379503A - ONt = 59.115325927734375 - OFFt = 276.0409851074219 - DC = 0.1763813599247232
     [java] 333793 - Ack received from 1a57
     [java] 380522A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 337796 - Turned Off
     [java] 337798 - Next on after: 24971
     [java] 384523A - ONt = 64.15875244140625 - OFFt = 276.0409851074219 - DC = 0.18859142250865987
     [java] 360513 - Forwarding packets from cache
     [java] 360548 - Data sent, cache empty
     [java] 362778 - Turned On
     [java] 409503A - ONt = 64.16970825195312 - OFFt = 301.13720703125 - DC = 0.17565971397559157
     [java] 363832 - Ack received from 1a57
     [java] 410569A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 367795 - Turned Off
     [java] 367797 - Next on after: 24972
     [java] 414522A - ONt = 69.2080078125 - OFFt = 301.13720703125 - DC = 0.18687431358252896
     [java] 390552 - Forwarding packets from cache
     [java] 390588 - Data sent, cache empty
     [java] 392779 - Turned On
     [java] 439504A - ONt = 69.2191162109375 - OFFt = 326.2319641113281 - DC = 0.17503837934777836
     [java] 393872 - Ack received from 1a57
     [java] 440601A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 397797 - Turned Off
     [java] 397799 - Next on after: 24970
     [java] 444524A - ONt = 74.264404296875 - OFFt = 326.2319641113281 - DC = 0.18543090563353504
     [java] 420591 - Forwarding packets from cache
     [java] 420626 - Data sent, cache empty
     [java] 422778 - Turned On
     [java] 469503A - ONt = 74.27557373046875 - OFFt = 351.33514404296875 - DC = 0.1745152803459413
     [java] 423910 - Ack received from 1a57
     [java] 470640A - Received data to FWD, saved in cache 5 elements [12b5, 12b5, 1519, 1154, 1a57, ]
     [java] 427795 - Turned Off
     [java] 427797 - Next on after: 24972
     [java] 474522A - ONt = 79.32028198242188 - OFFt = 351.33514404296875 - DC = 0.18418502865384842
     [java] 450630 - Forwarding packets from cache
     [java] 450667 - Data sent, cache empty
     [java] 452779 - Turned On
     [java] 499504A - ONt = 79.3319091796875 - OFFt = 376.4335632324219 - DC = 0.17406300823936594
     [java] 453948 - Ack received from 1a57
     [java] 500677A - Received data to FWD, saved in cache 4 elements [12b5, 1519, 1154, 1a57, ]
     [java] 457796 - Turned Off
     [java] 457798 - Next on after: 24971
     [java] 504523A - ONt = 84.37429809570312 - OFFt = 376.4335632324219 - DC = 0.18310082178833137
     [java] 480670 - Forwarding packets from cache
     [java] 480706 - Data sent, cache empty
     [java] 482778 - Turned On
     [java] 529503A - ONt = 84.38543701171875 - OFFt = 401.536865234375 - DC = 0.17366034985770631
     [java] 483985 - Ack received from 1a57
     [java] 530714A - Received data to FWD, saved in cache 3 elements [12b5, 1519, 1154, ]
     [java] 487795 - Turned Off
     [java] 487797 - Next on after: 24972
     [java] 534522A - ONt = 89.42852783203125 - OFFt = 401.536865234375 - DC = 0.182148332845805
     [java] 510708 - Forwarding packets from cache
     [java] 510743 - Data sent, cache empty
     [java] 512778 - Turned On
     [java] 559503A - ONt = 89.43923950195312 - OFFt = 426.6397705078125 - DC = 0.17330532295870876
     [java] 514031 - Ack received from 1a57
     [java] 560760A - Received data to FWD, saved in cache 4 elements [12b5, 1a57, 1519, 1154, ]
     [java] 517795 - Turned Off
     [java] 517797 - Next on after: 24972
     [java] 564522A - ONt = 94.48202514648438 - OFFt = 426.6397705078125 - DC = 0.18130507289156278
     [java] 540747 - Forwarding packets from cache
     [java] 540783 - Data sent, cache empty
     [java] 542779 - Turned On
     [java] 589504A - ONt = 94.49310302734375 - OFFt = 451.7262878417969 - DC = 0.1729947793998799
     [java] 544077 - Ack received from 1a57
     [java] 590807A - Received data to FWD, saved in cache 5 elements [1a57, 12b5, 1519, 1154, 1154, ]
     [java] 547796 - Turned Off
     [java] 547798 - Next on after: 24971
     [java] 594523A - ONt = 99.53125 - OFFt = 451.7262878417969 - DC = 0.18055308665650222
     [java] 570786 - Forwarding packets from cache
     [java] 570823 - Data sent, cache empty
     [java] 572778 - Turned On
     [java] 619503A - ONt = 99.54293823242188 - OFFt = 476.8191223144531 - DC = 0.1727090401092182
     [java] 574116 - Ack received from 1a57
     [java] 620846A - Received data to FWD, saved in cache 4 elements [1a57, 12b5, 1519, 1a57, ]
     [java] 577795 - Turned Off
     [java] 577797 - Next on after: 24972
     [java] 624522A - ONt = 104.58651733398438 - OFFt = 476.8191223144531 - DC = 0.17988562580374248
     [java] 600826 - Forwarding packets from cache
     [java] 600861 - Data sent, cache empty
     [java] 602778 - Turned On
     [java] 649503A - ONt = 104.59759521484375 - OFFt = 501.919189453125 - DC = 0.17245622521741852
     [java] 604155 - Ack received from 1a57
     [java] 650885A - Received data to FWD, saved in cache 5 elements [12b5, 1519, 1154, 1154, 1a57, ]
     [java] 607795 - Turned Off
     [java] 607797 - Next on after: 24972
     [java] 654522A - ONt = 109.6416015625 - OFFt = 501.919189453125 - DC = 0.17928160727965753
     [java] 630865 - Forwarding packets from cache
     [java] 631905 - Data send fail-IO: com.sun.spot.peripheral.NoRouteException: [LowPan] received a NoAckException on route to 0014.4F01.0000.15BD through 0014.4F01.0000.15BD
     [java] 631917 - Turned On
     [java] 634192 - Ack received from 1a57
     [java] 680921A - Received data to FWD, saved in cache 3 elements [12b5, 1519, 1a57, ]
