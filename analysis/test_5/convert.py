spots = ['sink','5812','1193','1A57','1579','16D7','12B5','1154','1519']
spotsID = {'5812':1,'1193':2,'1A57':3,'1579':4,'16D7':5,'12B5':6,'1154':7,'1519':8}
counter = [0,0,0,0,0,0,0,0,0]
limit = 90
i = 0

for spot in spots:
	f = open(spot + '-trace.txt', 'r')
	fRes = open(spot + '-dc.txt', 'w')
	start = False
	scheduleReset = 0
	initialAbsoluteTime = 0
	prevRadioState = 'off'
	lastFwd = 0

	for line in f:
		if not start:
			if line == '-run-spotclient-once:\n':
				start = True
		else:
			newLine = str(i) + ';' + spot + ';'

			if line[:12] == '     [java] ':
				l = line[12:]
				words = l.split(' - ')
				if len(words)>=2:
					if words[1] == 'Received schedule from parent\n':
						scheduleReset = long(words[0][:-1]) # reset absolute starting value for new schedule
						if initialAbsoluteTime == 0:
							initialAbsoluteTime = long(words[0][:-1])

					if words[1][:6] == 'ONt = ': # save Duty Cycle values
						time = str(( - initialAbsoluteTime + long(words[0].replace('A','')))/(1000.0 * 60.0))
						dc = str(float(words[3][5:]))

						newLine += time + ';'
						newLine += dc + ';;;\n'

						fRes.write(newLine)

					if (words[1][:10] == 'Turned Off') or (words[1][:9] == 'Turned On'): # save Radio Turn On/Off values
						time = str((scheduleReset - initialAbsoluteTime + long(words[0]))/(1000.0 * 60.0))

						begin = str(i) + ';' + spot + ';'
						if words[1][:10] == 'Turned Off' and prevRadioState == 'on':
							newLine += time + ';;'
							newLine += str(2 * i) + ';;\n'

							newLine += begin
							newLine += time + ';;'
							newLine += str((2 * i) - 1) + ';;\n'
							prevRadioState = 'off'
							#print (newLine)
							#print("switch on")
						elif words[1][:9] == 'Turned On' and prevRadioState == 'off':
							newLine += time + ';;'
							newLine += str((2 * i) - 1) + ';;\n'

							newLine += begin
							newLine += time + ';;'
							newLine += str(2 * i) + ';;\n'
							prevRadioState = 'on'
							#print (newLine)
							#print("switch off")
						else:
							continue

 						fRes.write(newLine)

 					if words[1] == "Forwarding packets from cache\n":
 						time = (scheduleReset - initialAbsoluteTime + long(words[0]))/(1000.0 * 60.0)
 						if lastFwd != 0:
 							newLine += str(lastFwd) + ";;;1;\n"
 							fRes.write(newLine)

 						lastFwd = time

 					if words[1] == "Data sent, cache empty\n":
 						time = (scheduleReset - initialAbsoluteTime + long(words[0]))/(1000.0 * 60.0)
 						lastFwd = 0
						newLine += str(time) + ";;;2;\n"
						fRes.write(newLine)

 					if words[1] == 'Packets':
 						for k in range(2,len(words)):
 							time = words[0]
 							seconds = long(time)/(1000.0)
 							minutes = seconds/(60.0)
 							s = hex(int(words[k]))[2:].upper()
 							idS = spotsID[s]

 							l = newLine + str(minutes) + ';' + s + ';' + str(idS) + ';' 
 							for j in range(0,idS - 1):
 								l += ";"
 							l2 = l + str(limit) + ";" # just received a package -> reset line to 90
 							l += str(limit - (seconds - counter[idS])) + ";" 
 							#if idS == 1 and counter[idS] != 0:
 							#	print seconds, counter[idS], limit - (seconds - counter[idS])
 							for j in range(idS,8):
 								l += ";"
 								l2 += ";"
 							l += "\n"
 							l2 += "\n"
 							if counter[idS] != 0:
 								fRes.write(l)
 							fRes.write(l2)
 							counter[idS] = seconds

	f.close()
	i += 1
	fRes.close()
