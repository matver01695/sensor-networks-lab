\documentclass[journal]{IEEEtran}
\usepackage[pdftex]{graphicx}
\graphicspath{{./images/}}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{fixltx2e}
\DeclareGraphicsExtensions{.png,.pdf}
\usepackage[cmex10]{amsmath}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
\title{Cross-layer solution for a wireless sensor network monitoring a greenhouse}
\author{Andrea~Zaccara, Matthias~Verstappen}%

\maketitle

\begin{abstract}
A wireless sensor network is an important technology in the context of environmental monitoring, as it allows the creation of large-scale networks with high temporal and spatial resolution.
However, the duration of the network is still an issue that needs to be tackled in every solution, depending on the structure of the network, the requirements and the environment they will be placed in.

In this paper we propose a cross-layer solution for the optimization of a sensor network used for monitoring in the context of a greenhouse. We discuss our solution in detail and evaluate it by looking at a weighted average of the Duty Cycle for each node in the network. 
\end{abstract}

\section{Introduction}
\label{sec:introduction}
Wireless sensor network is an emerging technology that is becoming more relevant as the components required to build them are becoming more accessible. A single sensor component can be very cheap, and it is easy to create big networks with small investment. 

However, as big wireless sensor networks are placed in an open environment, many factors need to be taken in consideration, such as an harsh and/or noisy environment, unpredictable events and network longevity. 
In previous studies these issues have been studied and documented in different context\cite{ewsn}, however in this paper we only focus on the longevity of the network.

In fact an effective monitoring network is useful as long as it is able to maintain enough coverage. They are expected to last for the longest time possible without manual intervention, which would nullify the previous advantages. 

To optimize its durability and their usefulness the most prominent factor is often relative to the radio being turned ON. To measure the impact of an implementation a direct or derivative measure of the Duty Cycle is often used, where the Duty Cycle is defined as the ratio between ON and OFF. 

In this paper we discuss our solution for the implementation of a wireless sensor network in the context of a greenhouse. We developed a prototype application using a set of SunSPOT's. The network  has a fixed topology and we are interested in minimizing the Duty Cycle, while maintaining a good sampling rate, to extend the usefulness of the solution.

Our implementation is based on the underlying SunSPOT's implementation of the Physical and MAC layers, which was not changed as our objective was the optimization via a cross-layer solution with the focus on a higher layer approach.

In section \ref{sec:network} we give an overview on the Networking layer implementation. In section \ref{sec:application} we look in detail at the solution implemented in the application layer. In section \ref{sec:metric} we look at the metric we defined to evaluate the efficiency of the network. In section \ref{sec:analysis} we compare the result obtained in two experiments with different parameters on the synchronization algorithm. In section \ref{sec:weak} we discuss the weak and strong points to our solution. Finally, we conclude in section \ref{sec:conclusion}.

\section{Network layer}
\label{sec:network}
The network is composed of eight monitor nodes and one sink node, as shown in figure \ref{fig:network}.
The network layer was defined using a fixed routing table, to cope with the static topology of the network. Each node for every destination in the network is associated with the next node to go through, and the total number of hops required to reach it. 

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.33\textwidth]{network}
 		\caption{The network topology}
 		\label{fig:network}
    \end{center}
\end{figure}

\section{Application layer}
\label{sec:application}
In our application layer we implemented a scheduling protocol inspired by the multi-hop TDMA and S-MAC protocols\cite{TDMA}. This protocol manages the transfer of packages between nodes over the SunSPOT's 6LowPAN protocol and makes sure that each node is able to successfully send and receive data with minor overhead on the Duty Cycle. 

This protocol defines a scheduling for when the radio should be ON, in each node of the network, using the information sent by the parent. 
The tree structure of the network is defined with the sink as the root node, which initializes the network by sending the first schedule to its direct child nodes (1,2). The propagation is done recursively for each node with at least one child node. The details are shown in section \ref{sec:scheduling}.

As the synchronization is done on the application layer, a certain difference in the clock speed of adjacent nodes is to be expected. This can lead to non-synchronized schedules and subsequent packet loss, which is managed by inserting self-healing functionality in both the child and parent nodes, as discussed in section \ref{sec:resync}.

To optimize the radio usage, we also implemented a caching system for packets, as discussed in section \ref{sec:aggregation}. 
All packets received and generated are saved in the cache, and then forwarded to the upper layer at the scheduled time for that node. 

\subsection{Messages between nodes}
\label{sec:messages}
In this protocol we use three types of packets, identified by the first value of the packet. These types are: Data packet (type=0), Schedule packet (type=1), Ack packet (type=2).

The Data and Ack packet have the same data header, containing the address of the sender. However, the Ack packet does not contain any other information.

The Data packet, after the type value and the address of the sender, contains one or more sets of the values sent by the Monitor nodes:
\begin{itemize}
	\item the address of the node which sampled the data,
	\item light and temperature, the current values sampled from the environment.
\end{itemize}
The Data packets, as discussed in section \ref{sec:aggregation}, are defined as an aggregation of multiple messages. 

The Schedule packet, after the type value, contains the values of:
\begin{itemize}
	\item the address of the node that should use the schedule,
	\item message delay, the time the receiving node should wait before sending the first Data packet for this schedule.
\end{itemize}


\subsection{Scheduling on the Application Layer}
\label{sec:scheduling}
The monitor nodes, after starting the monitor application, will start sampling the Data from the environment and keep the radio ON, without sending anything. In figure \ref{fig:slayer} the protocol is shown for a network composed of only two monitor nodes, directly connected to the sink. The listening interval is defined as 4 seconds and the sleep interval as 26 seconds, for a total Frame of 30 seconds.

The actual protocol is started by the sink node, which sends to all its direct child nodes a Schedule packet, as shown in figure \ref{fig:slayer_1}.
This packet defines when these child nodes should send their Data to the sink. The schedule from the sink is defined in such way that the nodes are spread out in the Frame. 

The child nodes must confirm the schedule after a small delay with an Ack Packet (figures \ref{fig:slayer_2} and \ref{fig:slayer_3}). 
The parent node then flags the child nodes as synchronized. 
If after 4 seconds some acknowledgments have not been received, the parent node will resend the schedule to all the non-compliant nodes, until it receives all their acknowledgments. 
The Data packets are also considered as Ack packets, to cope with possible collision during the sending of the ack of one or more child nodes, and to optimize the radio usage by sending directly the data, instead of two separate packets.

After the first Data packet is sent, the child nodes will use the 30 second Frame as interval, between the sending of each Data packets.

\begin{figure}[h]
    \caption{Initialization and first cycle}
    \label{fig:slayer}
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer1}
         \caption{Sink starts the communication, sending the schedules}
         \label{fig:slayer_1}
    \end{subfigure}%
	~~
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer2}
         \caption{Child1 sends an ack, then puts the radio to sleep}
         \label{fig:slayer_2}
    \end{subfigure}%
    
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer3}
         \caption{Child2 sends an ack, then puts the radio to sleep}
         \label{fig:slayer_3}
    \end{subfigure}%
	~~
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer4}
         \caption{Child1 sends data at the requested time}
         \label{fig:slayer_4}
    \end{subfigure}%
    
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer5}
         \caption{Child2 sends data at the requested time}
         \label{fig:slayer_5}
    \end{subfigure}%
	~~
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer6}
         \caption{Child1 turns the radio on, to check for a new schedule}
         \label{fig:slayer_6}
    \end{subfigure}%
    
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer7}
         \caption{Child2 turns the radio on, to check for a new schedule}
         \label{fig:slayer_7}
    \end{subfigure}%
	~~
	\begin{subfigure}[b]{0.23\textwidth}
         \includegraphics[width=\textwidth]{singleLayer8}
         \caption{The cycle repeats every 30 seconds}
         \label{fig:slayer_8}
    \end{subfigure}%
\end{figure}

After sending the Ack packet, the child nodes turn off the radio for the next 25 seconds, a time defined as a fraction of the frame after some practical tests. 
Then, when the parent scheduled time arrives, the nodes send the Data packets (figure \ref{fig:slayer_4} and \ref{fig:slayer_5}). 

Finally, they will turn on the radio for 4 seconds, to listen for possible changes in the schedule (figure \ref{fig:slayer_6} and \ref{fig:slayer_7}) and start back on the next frame cycle (figure  \ref{fig:slayer_8}).

In the complete network the schedule propagation is executed recursively as a sub-step, by all nodes with one or more child nodes. This additional step will be executed after the monitor node receives its own schedule and responds with an ack to the parent node. 

The full network schedule is constructed incrementally from the root to the leaves. 
An overview of the complete scheduling algorithm is shown in figure \ref{fig:activity}. 

\begin{figure}
	\includegraphics[width=0.47\textwidth]{scheduleActivity}
    \caption{The activity diagram for the scheduling algorithm}
    \label{fig:activity}
\end{figure}

In general, after a monitor node receives its schedule, it also defines a new schedule for its child nodes (if any). This new schedule is more restrictive, as the monitor nodes are going to turn off their radio in the periods outside of the schedule, compared to the sink which remains always ON. In particular, it has been set as a range of 4 seconds, divided between the child nodes, in our case two at most. 

So, for a Monitor with two child nodes the first half of the wake period is used to receive packets from the first child, while the second half is used for the second one.

%\begin{figure}
%	\includegraphics[width=0.47\textwidth]{sn_timeline}
%    \caption{The frame for a layer between monitors nodes}
%    \label{fig:timeline:1}
%\end{figure}

\subsection{Re-synchronization}
\label{sec:resync}

After the setup stage each node cycles from ON and OFF listening for Data or Schedule packets. For each listening interval, every parent node is going to reset the acknowledgment flag for all its child nodes. If the parent doesn't receive a Data or Ack packet from all of its child nodes, it will go back to sending the schedule to the missing ones every 4 seconds. The new schedules are synchronized to the previous one, as the parent will always refer to the starting time in which it  was defined.  

On the other hand, if a child node is not able to send a packet due to de-synchronization or to collisions, it will turn ON the radio and wait for the updated schedule from the parent.

This behavior allows a fast re-synchronization with minimal differences on the Duty Cycle of both parent and child node, as they both act in case of a send failure.

The 4 seconds listening interval will also be used for nodes with only one or no child nodes, as this time can be used to check if a parent node is sending a new Schedule. 
This configuration leads to an average Duty Cycle measure of around 14\%, resulting in the radio being in sleep mode for around 86\% of the time in the best case. This could be improved by decreasing the listening interval to less than 4 seconds, but this would also lead to more collisions and faster de-synchronization. As a consequence there would be more parent nodes with the radio ON waiting to get their child nodes synchronized again, so the average Duty Cycle would be worse.


\subsection{Packet aggregation}
\label{sec:aggregation}
As the defined schedule is decentralized, the Data packets from a node to the sink would almost always get lost in the second hop of the Mesh Under transmission, implemented in the SunSPOT's 6LowPAN protocol. That is, a node sends its message only based on the listening time of its parent. So, if the parent forwards the packets immediately to the next parent, those packets are going to be lost most of the time as the next layer schedule is going to be defined differently. 
Instead the packets are sent only to the upper layer in every time frame instead of sending them directly to the sink, which would require a scheduling where all nodes in the path from that node to the sink have the radio ON.

We implemented the sending of Packets as a single hop transmission between two layers. The packet is received by the application layer of the parent, which stores it in a local cache and forwards it with its own Data packet. 
This cache is also used by default whenever a Packet is generated. The data is stored there and is sent to the parent layer only when the schedule specifies it.
 
Since the Data generation is random, it may happen that in an Frame a node doesn't generate any packet. In this case, if it has an empty cache it's going to send an Ack packet, so the parent will still be notified that the node is alive and won't start a re-sync.



\section{Duty Cycle evaluation metric}
\label{sec:metric}
For the evaluation of our solution, and in general for any wireless sensor network with a fixed topology, we propose a weighted sum of the Duty Cycle. 
It is a valid metric only when each node can reach the sink using a single path.
This value is an indicator for the importance of the node in the network, and it is determined by three factors: \textit{distance from sink}($S_d$), \textit{number of child nodes}($C_a$) and \textit{number of direct child nodes}($C_d$).

These values are evaluated for each node, and then summed using the following formula \ref{eqn:weigth}, which normalizes these factors and returns a value between 0 and 1.

\begin{equation}
\label{eqn:weigth}
w_i = 0.35 * S_d + 0.45 * \frac{C_a + 1}{\max C_a + 1}  + 0.2 * \frac{C_d+1}{\max C_d + 1}
\end{equation}

The weights have been defined with the following considerations in mind.
\begin{itemize}
	\item The highest importance should be given to nodes with many nodes depending on it. This does not consider the distance between the nodes.
	\item More importance should also be given to nodes with less distance to the sink, as they have less dependency on other nodes.
	\item The least important, but still considered in our calculation is the number of child nodes directly connected. In fact, we want to maintain alive conjunction points for a longer time, compared to sequential paths with the same factors.
\end{itemize}
 
The values for the network in figure \ref{fig:network} are presented in table \ref{tab:t1}. This gives an order of importance for each node, and consequently the order in which the nodes should stop working. 

\begin{table}[h]
\caption{Factors and final weight for the example network}
\label{tab:t1}
\begin{center}
    \begin{tabular}{| l | c | c | c | r |}
    \hline
    \bfseries Node & \bfseries Sink\textsubscript{Distance} & \bfseries \#\textsubscript{Children} & \bfseries \#\textsubscript{DirectChildren} & \bfseries Final Weight \\ \hline
    1 & 4 & 4 & 1 & 0.933 \\ \hline
    2 & 4 & 2 & 2 & 0.820 \\ \hline
    3 & 3 & 3 & 2 & 0.823 \\ \hline
    4 & 3 & 0 & 0 & 0.419 \\ \hline
    5 & 3 & 0 & 0 & 0.419 \\ \hline
    6 & 2 & 0 & 0 & 0.331 \\ \hline
    7 & 2 & 1 & 1 & 0.488 \\ \hline
    8 & 1 & 0 & 0 & 0.244 \\ \hline
    \multicolumn{4}{|r|}{Total} & 4.477 \\
    \hline
    \end{tabular}
\end{center}
\end{table}

So, for example node 1 is directly connected to the sink and has the highest number of child nodes in the network. This gives it the highest value in the network, however it's still not 1 as only one of the child nodes is directly connected to it.

Also, node 3 has a slightly higher value than node 2, justified by the higher number of dependent nodes even with a longer distance from the sink.

In the next section we use these weights to get a rating of the network longevity, based on the Duty Cycle of each node. 

\section{Performance analysis}
\label{sec:analysis}
To evaluate our cross-layer solution we conducted a series of tests measuring the Duty Cycle and radio ON/OFF timings at monitoring nodes and also the Data packet arrival times at the sink node. Using the results we can evaluate the performance of our solution.

In section \ref{sec:duty_cycle} we discuss how the Duty Cycle of the monitor nodes evolves in time with two different parameters of the listening interval. In section \ref{sec:arrival_t} we look at the arrival times of the Data packet at the sink for each monitor node. In section \ref{sec:dc_comp} we look at the weighted Duty Cycles for these experiments. 

\subsection{Analysis of the Duty Cycle}
\label{sec:duty_cycle}
Upon testing our solution we logged the radio ON/OFF timings and the Duty Cycle, each time a monitor node turned ON or OFF. This allows us to plot the Duty Cycle of each monitor node in function of time. For each node we consider the starting time as the timestamp at which the first schedule is received. Figure \ref{fig:dc5} shows these values. 

The saw-tooth wave like plot for every node is the result of the radio turning on for the listening interval, and shutting down for the rest of the Frame.

\begin{figure}[h]
    \includegraphics[width=0.47\textwidth]{dc-5s}
    \caption{Duty cycle of the monitor nodes, for interval of 5s}
    \label{fig:dc5}
\end{figure}

Every node starts with a Duty Cycle of 1, since all nodes turn their radio ON at boot time, while waiting for the first schedule from their parent. 
When the sink is turned ON, the schedule propagation starts and the Duty Cycle starts dropping, as all nodes are initialized with their ON/OFF schedule. 
After approximately 5 minutes all nodes have reached their average Duty Cycle of around 80\% OFF and are stable. 

We notice that at 2.5 minutes, node '1A57''s Duty Cycle rises and drop back down after some time. 
This can be explained by fact that this node needs to send new schedules to its child nodes, due to a de-synchronization or loss of packets for collisions. The same happens for node '1193' at 4 minutes. 

We made a similar experiment with the listening interval set to 4 seconds. The result on the Duty Cycle is shown in figure \ref{fig:dc4}. It has a similar behavior and allows most of the nodes to reach a higher level of Duty Cycle, up to 85\%. 
However, it is less stable in general, as problems with the different schedule have a stronger impact on some nodes. We can see how node '1193' reaches 60\% after 35 minutes, where it keeps having synchronization problems with its child nodes.

\begin{figure}[h]
    \includegraphics[width=0.47\textwidth]{dc-4s}
    \caption{Duty cycle of the monitor nodes, for interval of 4s}
    \label{fig:dc4}
\end{figure}

To get a better view on how long the radio of each node is in the ON state, figure \ref{fig:radio} shows how long every node has its radio ON and OFF in a shorter interval of 10 minutes, for the experiment with a 5s listening interval. Here we can clearly see how node '1A57' keeps its radio ON for longer periods of time between minute 1 and 3, as the nodes adjust their schedules.

\begin{figure}[t]
    \includegraphics[width=0.47\textwidth]{radio}
    \caption{Radio state over time}
    \label{fig:radio}
\end{figure}

\subsection{Analysis of Arrival times}
\label{sec:arrival_t}
To show that for each monitor node in the network, all of it's generated packets arrive at the sink in a maximum time of 90 seconds, we plotted the arrival timings in figure \ref{fig:arr_full}, and in the detailed snapshot of figure \ref{fig:arr_zoom}. This figures show the amount of time that elapsed since the sink got the latest packet for each monitor node.
If for some node the timer goes negative, this indicates the time between two subsequent packets is greater than 90 seconds and the led light of this node will turn red. When this happens, the requirements are not met. 

\begin{figure}[h]
    \includegraphics[width=0.47\textwidth]{arr_full}
    \caption{Packet arrival timings at the sink node}
    \label{fig:arr_full}
\end{figure}

Looking at figure \ref{fig:arr_full} we see that most packets are received in 30 seconds. Sometimes however, up to 60 seconds are needed to get the packet from a monitor node to the sink. This is possible, due to the fact that the sampled data can be generated in intervals randomly longer than the 30 seconds Frame, up to 33 seconds. This can leave a frame uncovered by a node, however as the next sending frame happens, the packet is transmitted.

\begin{figure}
    \includegraphics[width=0.47\textwidth]{arr_zoom}
    \caption{10 minute close-up of figure \ref{fig:arr_full}}
    \label{fig:arr_zoom}
\end{figure}

It's clear that all packets are received within the requested 90 seconds interval. However if we look at the implementation of our solution we notice that, in the worst case scenario, it is possible that a packet from the last layer arrives with a 120 seconds delay at the sink. The reason is that in the worst case scenario, it takes 30 seconds for a packet to pass through a layer, and there are 4 layers in total. 

\subsection{Weighted Duty Cycle comparison}
\label{sec:dc_comp}
Using the metric defined in section \ref{sec:metric}, we give here an evaluation of our system with the listening interval parameter defined in experiment 1 as 5 seconds, and in experiment 2 as 4 seconds.

\begin{table}[h]
\caption{Solution comparison (Radio OFF percentage)}
\label{tab:t2}
\begin{center}
    \begin{tabular}{| l | c | c | c | r |}
    \hline
    \bfseries Experiment & \bfseries Average & \bfseries Weighted average \\ \hline
    1 & 81,65\% & 81,62\% \\ \hline
    2 & 82,22\% & 80,58\% \\
    \hline
    \end{tabular}
\end{center}
\end{table}

Table \ref{tab:t2} shows the comparison of the average value of all nodes and the value obtained by applying the weighted average. We notice that, if we take the aveage Duty Cycle for each node, experiment 2 performs better than experiment 1. When we apply our metric and consider the weighted average though, the first experiment provides a better overall Duty Cycle. So we would use a 5 second interval (as in experiment 1) because the overall Duty Cylce for each monitor node is much more stable (see figure \ref{fig:dc5} vs figure \ref{fig:dc4}) and our weighted average Duty Cycle is also slightly better.

Moreover, given the perturbation that can happen on the second experiment, the values used do not completely represent the solution, which could get a worse Duty Cycle value in an extended period of time.

\section{Weak and strong points}
\label{sec:weak}
As of now, this solution leads to good results in general, as it achieves a similar Duty Cycle for each node, maintaining a stable value for each node.
This leads to a network where each node essentially has the same lifespan. This can be a good solution in certain cases, however it is not optimal as it does not take in consideration the position of the nodes in the topology or the number of direct and non-direct child nodes it is connected to. 

In fact a de-synchronization near the sink node has a greater impact on the full network compared to a de-synchronization near the leaves, as a re-sync resets the schedule for all of the child nodes, requiring some periods of longer listening time.
This could be partially avoided by differentiating the listening interval between higher and lower nodes, or by maintaining the previous schedule for the layers below the non-synced one, if they were not affected by the de-synchronization.

Also, as a node is removed from the network due to external causes, the connected nodes will die faster as both parent and child nodes will maintain the radio ON to check for the missing node, without a timeout. This is only a problem for external causes, as the normal behavior of the network will lead to an equal battery discharge for every node, leading to a little impact on the lifetime of the network.

\section{Conclusion}
\label{sec:conclusion}
As the presented solution uses the application layer for managing the scheduling, with a TDMA-like approach, the synchronization is achieved with a worse Duty Cycle than what could be done by applying a TDMA protocol directly to the MAC layer. 

However, this application level solution still allows for the system to reach a reasonable longevity with additional features such as self-healing functionalities. It is also possible to consider various optimizations that could lead to a better solution.

More experiments could show that the same protocol gives better results with a time frame of 40 seconds, where the data is always generated at least once during a frame. Also, the scheduling for adjacent layers could be optimized by moving the sending period to the end of the Frame, leading to a more direct data flow. Finally, the listening interval could be dependent on the position of the network, and the nodes depending on it to reach the sink. We leave these considerations as a starting point for future works.



\begin{thebibliography}{1}

\bibitem{ewsn}
P.~Corke, T.~Wark, R.~Jurdak, W.~Hu, P.~Valencia, D.~Moore. 
\emph{Environmental wireless sensor networks}, Proc. IEEE 2010, 98, 1903–1918.

\bibitem{TDMA}
L.F.W.~van Hoesel, T.~Nieberg, H.J.~Kip, P.J.M.~Havinga. 
\emph{Advantages of a TDMA based, energy-efficient, self-organizing MAC protocol for WSNs}, Vehicular Technology Conference, 2004. VTC 2004-Spring. 2004 IEEE 59th, pp. 1598-1602 Vol.3.


\end{thebibliography}

\end{document}


