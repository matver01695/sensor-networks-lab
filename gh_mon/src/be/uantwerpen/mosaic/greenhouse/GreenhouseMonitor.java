package be.uantwerpen.mosaic.greenhouse;

import be.uantwerpen.mosaic.greenhouse.DataSender;

import com.sun.spot.peripheral.Spot;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.resources.transducers.ITemperatureInput;
import com.sun.spot.sensorboard.EDemoBoard;

import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;



/**
 * The GreenhouseMonitor generates a data sample that needs to be sent to the sink at a semi-random interval.
 * The interval can be controlled by setting the "greenhouse.min_interval" and "greenhouse.max_interval" system properties
 *
 * @author Bart Sas
 *
 */
public class GreenhouseMonitor {

        private static final String MIN_INTERVAL_KEY     = "greenhouse.min_interval";
        private static final long   DEFAULT_MIN_INTERVAL = 27; // seconds
        private static final String MAX_INTERVAL_KEY     = "greenhouse.max_interval";
        private static final long   DEFAULT_MAX_INTERVAL = 33; // seconds

        private Timer  timer;
        private Random rng;

        private DataSender sender;
        private short      address;
        private long       min_interval;
        private long       max_interval;

        private ILightSensor      light_sensor;
        private ITemperatureInput temperature_sensor;

        private class SendMessage extends TimerTask {

                public SendMessage() {
                        super();
                }

                public void run() {
                        try {
                                final int    light_intensity = light_sensor.getValue();
                                final double temperature     = temperature_sensor.getCelsius();

                                final ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
                                final DataOutput            data_output   = new DataOutputStream(output_stream);

                                data_output.writeShort(address);
                                data_output.writeInt(light_intensity);
                                data_output.writeDouble(temperature);

                                final byte[] data   = output_stream.toByteArray();
                                final int    length = output_stream.size();

                                sender.sendData(data, length);
                        }
                        catch(final IOException exception) {
                                exception.printStackTrace();
                        }

                        // Schedule the next message.
                        scheduleMessage();
                }

        }

        private static long getLongParameter(final String key, final long default_value) {
                final String value = System.getProperty(key);

                if(value != null) {
                        try {
                                return Long.parseLong(value);
                        }
                        catch(final NumberFormatException exception) {
                                System.err.println(
                                        "Warning: could not parse value of "
                                                + key
                                                + " ("
                                                + value
                                                + "), using default value ("
                                                + String.valueOf(default_value)
                                                + ")"
                                );
                        }
                }
                else {
                        System.err.println(
                                "Warning: no value provided for "
                                        + key
                                        + ", using default value ("
                                        + String.valueOf(default_value)
                                        + ")"
                        );
                }

                return default_value;
        }

        /**
         * @param sender_init -
         */
        public GreenhouseMonitor(final DataSender sender_init) {
                timer = new Timer();
                rng   = new Random();

                sender       = sender_init;
                address      = (short)(Spot.getInstance().getRadioPolicyManager().getIEEEAddress() & 0xFFFFl);
                min_interval = getLongParameter(MIN_INTERVAL_KEY, DEFAULT_MIN_INTERVAL) * 1000l;
                max_interval = getLongParameter(MAX_INTERVAL_KEY, DEFAULT_MAX_INTERVAL) * 1000l;

                light_sensor       = EDemoBoard.getInstance().getLightSensor();
                temperature_sensor = EDemoBoard.getInstance().getADCTemperature();

                // Schedule the first message.
                scheduleMessage();
        }

        /**
         *
         */
        public void stop() {
                timer.cancel();
        }

        private void scheduleMessage() {
                final long duration = min_interval + ((max_interval != min_interval) ? (Math.abs(rng.nextLong()) % (max_interval - min_interval)):0);
                timer.schedule(new SendMessage(), duration);
        }

}
