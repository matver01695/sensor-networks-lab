package be.uantwerpen.mosaic.greenhouse;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.util.Vector;

public class AggregatorMonitor implements DataSender {
	private Vector pckCache;

	public AggregatorMonitor() {
		pckCache = new Vector();
	}

	public void sendData(byte[] data, int length) {
		// intercept generated messages and cache them
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		DataInput   data_input   = new DataInputStream(input);

		Vector pck = Packet.parseMultiplePackets(data_input);
		for (int i=0; i<pck.size(); i++){
			cachePacket((Packet) pck.elementAt(i));
		}
	}

	public boolean cacheEmpty(){
		return pckCache.size() == 0;
	}
	
	public Vector getCache(){
		Vector cache = pckCache;
		return cache;
	}

	public void emptyCache(){
		pckCache = new Vector();
	}

	public void cachePacket(Packet p){
		Vector pckTemp = new Vector();
		pckTemp.addElement(p);
		
		for (int i=0; i<pckCache.size(); i++){ // remove duplicate packets, keep only new ones
			if (((Packet) pckCache.elementAt(i)).address.id != p.address.id){
				pckTemp.addElement(pckCache.elementAt(i));
			}
		}
		pckCache = pckTemp;
		
		//pckCache.addElement(p);
		
	}

}
