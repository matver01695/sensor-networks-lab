package be.uantwerpen.mosaic.greenhouse;

import java.util.Timer;
import java.util.TimerTask;

import be.uantwerpen.mosaic.sensornetworks.IRadioMonitor;
import be.uantwerpen.mosaic.sensornetworks.IRadioMonitorListener;

import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.util.Utils;

/**
 * Controls the LEDs of the Demoboard for the GreenhouseMonitor Application
 *
 * @author Daniel van den Akker
 * @author Bart Sas
 */
public class LEDController implements IRadioMonitorListener
{
	
	private final ITriColorLED[]	BATTERY_LEDS;
	private final LEDColor[][]		BATTERY_COLORS;

	private final ITriColorLED		RADIO_STATUS_LED;
	private final LEDColor			RADIO_ENABLED_COLOR;
	private final LEDColor			RADIO_DISABLED_COLOR;

	private final ITriColorLED		MESSAGE_LED;
	private final LEDColor			MESSAGE_COLOR;
	private final long				MESSAGE_DELAY;
	
	private final IRadioMonitor		monitor;
	private static final int		BATTERY_TIMEOUT	= 1000;
	
	private DutyCycleThread			dutyCycleThread;
	private TimerTask				switch_led_off;		
	private Timer					timer;
	

	/**
	 * Creates a new radio controller
	 * @param monitor	- The RadioMonitor used to calculate the duty cycle
	 * @param battery_leds - the leds that are used to display the duty cycle 
	 * @param battery_colors - which colors are used to display the duty cycle
	 * @param radio_status_led - the led used to display the current state of the radio
	 * @param radio_status_on_color - the color of the radio_status_led when the radio is enabled
	 * @param radio_status_off_color - the color of the radio_status_led when the radio is disabled
	 * @param message_led - the led used to signal that a message has been sent
	 * @param message_color - the color used by the message_led
	 * @param message_delay - the number of ms that the message_led should remain on after a message has been sent
	 */
	public LEDController(IRadioMonitor monitor, ITriColorLED battery_leds[], LEDColor battery_colors[], ITriColorLED radio_status_led, LEDColor radio_status_on_color, LEDColor radio_status_off_color, ITriColorLED message_led, LEDColor message_color, long message_delay)
	{
		super();

		this.monitor = monitor;
		if(monitor == null)
			throw new NullPointerException("monitor may not be null");
		if (battery_leds == null)
			throw new NullPointerException("battery_leds may not be null");
		if (battery_colors == null)
			throw new NullPointerException("battery_colors may not be null");
		if (battery_colors.length < 1)
			throw new IllegalArgumentException("battery_colors must have at least size 1");
		if (radio_status_led == null)
			throw new NullPointerException("radio_status_led may not be null");
		if (radio_status_on_color == null)
			throw new NullPointerException("radio_status_on_color may not be null");
		if (radio_status_off_color == null)
			throw new NullPointerException("radio_status_off_color may not be null");
		if (message_led == null)
			throw new NullPointerException("message_led may not be null");
		if (message_color == null)
			throw new NullPointerException("message_color may not be null");

		BATTERY_LEDS = battery_leds;
		//do some freaky stuff to calculate the color schemes
		BATTERY_COLORS = new LEDColor[(battery_colors.length - 1) * BATTERY_LEDS.length + 1][];
		for (int i = 0; i < BATTERY_COLORS.length; i++)
		{
			BATTERY_COLORS[i] = new LEDColor[BATTERY_LEDS.length];
			for (int j = 0; j < BATTERY_LEDS.length; j++)
			{
				BATTERY_COLORS[i][j] = battery_colors[(i / BATTERY_LEDS.length) + ((i % BATTERY_LEDS.length) + j) / BATTERY_LEDS.length];
			}
		}

		RADIO_STATUS_LED = radio_status_led;
		RADIO_ENABLED_COLOR = radio_status_on_color;
		RADIO_DISABLED_COLOR = radio_status_off_color;

		MESSAGE_LED = message_led;
		MESSAGE_COLOR = message_color;
		MESSAGE_DELAY = message_delay;
		
		timer = new Timer();
		switch_led_off = null;
		
		this.dutyCycleThread = new DutyCycleThread();
		this.dutyCycleThread.start();
		monitor.addListener(this);
		radioStateChanged(monitor.isRadioCurrentlyEnabled());

	}

	private class DutyCycleThread extends Thread
	{
		private boolean	terminated	= false;

		public void run()
		{
			while (!terminated)
			{
				double dutyCycle = monitor.getDutyCylce();
				int level = Math.max(0, Math.min((int) Math.floor(dutyCycle * (BATTERY_COLORS.length)), BATTERY_COLORS.length - 1));
				for (int i = 0; i < BATTERY_LEDS.length; i++)
				{
					BATTERY_LEDS[i].setColor(BATTERY_COLORS[level][i]);
					BATTERY_LEDS[i].setOn(true);
				}
				Utils.sleep(BATTERY_TIMEOUT);
			}
		}

		public void Terminate()
		{
			terminated = true;
		}
	}
	
	private class SwitchLEDOff extends TimerTask
	{

		public void run()
		{
			MESSAGE_LED.setOff();
			switch_led_off = null;
		}

	}

	/**
	 * Terminate the LEDContoller
	 */
	public void Terminate()
	{
		if (dutyCycleThread != null)
			dutyCycleThread.Terminate();
		monitor.removeListener(this);
		if(switch_led_off != null)
		{
			switch_led_off.cancel();
			switch_led_off = null;
			MESSAGE_LED.setOff();
		}
		RADIO_STATUS_LED.setOff();
		for(int i = 0; i < BATTERY_LEDS.length; i++)
			BATTERY_LEDS[i].setOff();		
	}

	public void radioStateChanged(boolean radioEnabled)
	{
		if (RADIO_STATUS_LED != null)
		{
			RADIO_STATUS_LED.setColor(radioEnabled ? RADIO_ENABLED_COLOR : RADIO_DISABLED_COLOR);
			RADIO_STATUS_LED.setOn(true);
		}
	}
	
	/**
	 * Informs the LEDController that a message has been sent to the Sink
	 */
	public void signalMessageSent()
	{
		MESSAGE_LED.setColor(MESSAGE_COLOR);
		MESSAGE_LED.setOn();

		if (switch_led_off != null)
		{
			switch_led_off.cancel();
		}

		switch_led_off = new SwitchLEDOff();
		timer.schedule(switch_led_off, MESSAGE_DELAY);		
	}
	
	
	
}
