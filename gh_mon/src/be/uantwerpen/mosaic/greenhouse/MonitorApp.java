package be.uantwerpen.mosaic.greenhouse;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;

import be.uantwerpen.mosaic.sensornetworks.RadioMonitor;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.radio.ILowPan;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.peripheral.radio.LowPan;
import com.sun.spot.peripheral.radio.RadioPolicy;
import com.sun.spot.peripheral.radio.routing.interfaces.IRoutingManager;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ISwitchListener;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.SwitchEvent;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.service.BootloaderListenerService;

/**
 * The MIDLet for the GreenhouseMonitoring Application
 *
 * @author Bart Sas
 * @author Daniel van den Akker
 *
 */
public class MonitorApp extends MIDlet implements ISwitchListener, GenericSender
{

	private static final LEDColor	BLUE						= new LEDColor(0, 0, 100);
	private static final LEDColor	GREEN						= new LEDColor(0, 100, 0);
	private static final LEDColor	RED							= new LEDColor(100, 0, 0);
	private static final LEDColor	YELLOW						= new LEDColor(50, 50, 0);

	private static final String		SINK_ADDRESS_KEY			= "0014.4F01.0000.15BD";
	private static final int		DESTINATION_PORT			= 10;

	private static final int		MESSAGE_LED_INDEX			= EDemoBoard.LED2;
	private static final LEDColor	MESSAGE_LED_COLOR			= BLUE;
	private static final long		MESSAGE_LED_DELAY			= 500l;

	private static final int		RADIO_STATUS_LED_INDEX		= EDemoBoard.LED1;
	private static final int		RADIO_STATUS_SWITCH_INDEX	= EDemoBoard.SW1;
	private static final LEDColor	RADIO_ENABLED_COLOR			= RED;
	private static final LEDColor	RADIO_DISABLED_COLOR		= GREEN;

	private static final int		DUTY_CYCLE_STATUS_LEDS[]	= new int[] { EDemoBoard.LED4, EDemoBoard.LED5, EDemoBoard.LED6, EDemoBoard.LED7, EDemoBoard.LED8 };
	private static final LEDColor	DUTY_CYCLE_STATUS_COLORS[]	= new LEDColor[] { BLUE, GREEN, YELLOW, RED };

	private IRadioPolicyManager		radio_policy_manager;
	private ISwitch					radio_toggle_switch;
	private boolean					radio_enabled;

	private RadiogramConnection		connection_rec;
	private RadiogramConnection		connection_send;
	private GreenhouseMonitor		monitor;
	private ScheduleMonitor			scheduler;
	private AggregatorMonitor 		aggregator;
	
	private LEDController			led_controller;
	private boolean					app_running;

	private Thread thread;
	private volatile boolean running;
	private static final int LISTEN_PORT = 10;

	private long initialTime;

	/**
	 *
	 */
	public MonitorApp()
	{
		radio_policy_manager = null;
		radio_toggle_switch = null;
		radio_enabled = true;

		connection_rec = null;
		connection_send = null;
		monitor = null;
		led_controller = null;
		app_running = false;
		thread = new Thread(new ReceiverComponent());
		initialTime = System.currentTimeMillis();
	}
	
	public long getAbsoluteTime(){
		return System.currentTimeMillis() - initialTime;
	}

	public void startApp()
	{
		ILowPan lowPan = LowPan.getInstance();
		IRoutingManager newRM = new MyRoutingMgr();
		lowPan.setRoutingManager(newRM);

		radio_policy_manager = Spot.getInstance().getRadioPolicyManager();
		radio_policy_manager.setRxOn(radio_enabled);

		BootloaderListenerService.getInstance().start();
		final String sink_address = SINK_ADDRESS_KEY; //System.getProperty(SINK_ADDRESS_KEY);

		if(EDemoBoard.getInstance() != null)
		{
			final EDemoBoard demo_board = EDemoBoard.getInstance();
			radio_toggle_switch = demo_board.getSwitches()[RADIO_STATUS_SWITCH_INDEX];
			radio_toggle_switch.addISwitchListener(this);
			ITriColorLED leds[] = new ITriColorLED[DUTY_CYCLE_STATUS_LEDS.length];

			for(int i = 0; i < leds.length; i++)
				leds[i] = EDemoBoard.getInstance().getLEDs()[DUTY_CYCLE_STATUS_LEDS[i]];

			led_controller = new LEDController(	RadioMonitor.getInstance(),
					leds,
					DUTY_CYCLE_STATUS_COLORS,
					EDemoBoard.getInstance().getLEDs()[RADIO_STATUS_LED_INDEX],
					RADIO_ENABLED_COLOR,
					RADIO_DISABLED_COLOR,
					EDemoBoard.getInstance().getLEDs()[MESSAGE_LED_INDEX],
					MESSAGE_LED_COLOR,
					MESSAGE_LED_DELAY);

		}

		if (sink_address != null)
		{
			try
			{
				connection_send = (RadiogramConnection) Connector.open("radiogram://" 
						+ ((MyRoutingMgr)newRM).parent_addr + ":" + String.valueOf(DESTINATION_PORT));
				connection_send.setRadioPolicy(RadioPolicy.AUTOMATIC);

				aggregator = new AggregatorMonitor();
				monitor = new GreenhouseMonitor(aggregator);
				scheduler = new ScheduleMonitor(this, aggregator);
				app_running = true;
			}
			catch (final IOException exception)
			{
				System.err.println("Error: could not open datagram connection, not starting app");
				exception.printStackTrace();
			}
		}
		else
		{
			System.err.println("Error: address of sink not provided, not starting app");
		}

		try {
			connection_rec = (RadiogramConnection) Connector.open("radiogram://:"
					+ String.valueOf(LISTEN_PORT));
			connection_rec.setRadioPolicy(RadioPolicy.AUTOMATIC);

			running = true;

			thread.start();
		} catch (final IOException exception) {
			exception.printStackTrace();
			running = false;
		}
	}

	public void pauseApp()
	{
		// Ignore this event.
	}

	public void destroyApp(final boolean unconditional)
	{
		radio_toggle_switch.removeISwitchListener(this);
		if (app_running)
		{
			app_running = false;
			running = false;
			monitor.stop();
			if(led_controller != null)
				led_controller.Terminate();
			try
			{
				connection_rec.close();
				connection_send.close();
			}
			catch (final IOException exception)
			{
				// Ignore this exception...
			}
		}
	}

	public void switchPressed(final SwitchEvent event)
	{
		if (event.getSwitch() == radio_toggle_switch)
		{
			radio_enabled = !radio_enabled;
			radio_policy_manager.setRxOn(radio_enabled);
		}
	}

	public void switchReleased(final SwitchEvent event)
	{
		// Ignore this event...
	}

	// Send section --------------------------------------------------
	
	public boolean sendData(final byte[] data, final int length)
	{
		try
		{
			final Datagram datagram = connection_send.newDatagram(length);
			datagram.write(data, 0, length);

			connection_send.send(datagram);

			if(led_controller != null)
				led_controller.signalMessageSent();
			return true;
		} catch (final IOException exception) {
			System.out.println(scheduler.getScheduleTime() + " - Data send fail-IO: " + exception.toString());
		} catch(final Exception ex) {
			System.out.println(scheduler.getScheduleTime() + " - Data send fail: " + ex.toString());
		}
		return false;
	}

	public boolean sendSchedule(final byte[] data, final int length, Address address)
	{
		RadiogramConnection connection_child = null;
		try
		{
			connection_child = (RadiogramConnection) Connector.open("radiogram://" 
					+ address.toString() + ":" + String.valueOf(DESTINATION_PORT));
			final Datagram datagram = connection_child.newDatagram(length);
			datagram.write(data, 0, length);

			connection_child.send(datagram);
			
			if(led_controller != null)
				led_controller.signalMessageSent();
			return true;
		} catch (final IOException exception) {
			System.out.println(scheduler.getScheduleTime() + " - Schedule send fail-IO: " + exception.toString());

		} catch(final Exception ex) {
			System.out.println(scheduler.getScheduleTime() + " - Schedule send fail: " + ex.toString());
		} finally {
			if (connection_child != null)
				try { connection_child.close(); } catch (IOException e) {}
		}
		return false;
	}
	
	// Receiver section ---------------------------------------------
	
	public void dataReceived(byte[] data, int offset, int length){
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		DataInput   data_input   = new DataInputStream(input);
		try {
			short type = data_input.readShort();
			short addr = data_input.readShort();
			long address = scheduler.ADDRESS_PREFIX + addr;
			if (type==0){ // data packets
				scheduler.receivedAckBy(address);
				cacheData(data_input);
			}else if (type==1){ // schedule from top node
				if (addr==scheduler.getAddress()){
					System.out.println(getAbsoluteTime() + "A - Received schedule from parent");
					scheduler.resetTime();
					long ms_before_next_send = data_input.readLong();
					scheduler.scheduleSendAck();
					scheduler.scheduleSendMessage(ms_before_next_send);
				}
			}else if (type==2){ // schedule confirmation
				scheduler.receivedAckBy(address);
			}else{
				System.err.println("Unknown packet type");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cacheData(DataInput   data_input){
		Vector pck = Packet.parseMultiplePackets(data_input);

		System.out.print(getAbsoluteTime() + "A - Received data to FWD, saved in cache "+pck.size()+" elements [");
		for (int i=0; i<pck.size(); i++){
			aggregator.cachePacket((Packet) pck.elementAt(i));
			Address add = ((Packet)pck.elementAt(i)).address;
			
			System.out.print(add.getID() + ", ");
			//System.out.print(" - light: "+((Packet)pck.elementAt(i)).light);
			//System.out.println(" - temp: "+((Packet)pck.elementAt(i)).temp);
			
			scheduler.receivedAckBy(add.address, true);
		}
		System.out.println("]");
	}

	/**
	 * Receiver thread, listens to the radio 
	 * @author s0149717
	 *
	 */
	public class ReceiverComponent implements Runnable{

		public void run() {
			System.out.println(getAbsoluteTime() + "A - Started receiver thread");
			try {
				final Datagram datagram = connection_rec.newDatagram(connection_rec
						.getMaximumLength());

				while (running) {
					try {
						datagram.reset();
						connection_rec.receive(datagram);

						final byte[] data = datagram.getData();
						final int offset = datagram.getOffset();
						final int length = datagram.getLength();

						dataReceived(data,offset,length);
					} catch (final IOException exception) {
						exception.printStackTrace();
					}
				}
			} catch (final IOException exception) {
				exception.printStackTrace();
			}
		}

	}
}
