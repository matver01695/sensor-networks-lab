package be.uantwerpen.mosaic.greenhouse;

class Address{
	public final long address;
	public final short id;
	private final String string_address;
	private final String string_id;
	
	public Address (long addr){
		address = addr;
		
		String sAddr = "0000" + Long.toString(address,16);
		sAddr = sAddr.substring(sAddr.length()-16,sAddr.length());
		string_address = sAddr.substring(0, 4)+"."+sAddr.substring(4, 8)+"."
				+sAddr.substring(8, 12)+"."+sAddr.substring(12, 16);
		
		String s = Integer.toHexString((int) address);
		string_id = s.substring(s.length()-4);
		
		id = (short) Integer.parseInt(string_id,15);
	}
	
	public Address (short addr){
		string_id = Integer.toHexString(addr);
		id = addr;
		string_address = "0014.4F01.0000." + string_id;
		address = 0x00144F0100000000l + addr;
	}

	public String toString() {
		return string_address;
	}
	
	public String getID(){
		return string_id;
	}
}