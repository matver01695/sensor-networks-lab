package be.uantwerpen.mosaic.greenhouse;
import be.uantwerpen.mosaic.sensornetworks.IRadioMonitor;
import be.uantwerpen.mosaic.sensornetworks.RadioMonitor;

import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.peripheral.radio.LowPan;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

/**
 * The GreenhouseMonitor generates a data sample that needs to be sent to the
 * sink at a semi-random interval. The interval can be controlled by setting the
 * "greenhouse.min_interval" and "greenhouse.max_interval" system properties
 * 
 * @author Bart Sas
 * 
 */
public class ScheduleMonitor {

	public final long     ADDRESS_PREFIX        = 0x00144F0100000000l;
	private static final long SEND_INTERVAL = 30000; // seconds

	private Timer timerSender;
	private Timer timerRadio;
	private long startingTime;
	private boolean hasSchedule;
	private double initialDCOffset;

	private GenericSender sender;
	private AggregatorMonitor aggregator;
	private short address;

	//private long schedule_interval = 26000;
	private long listening_time = 5000;
	private final Vector children;
	long[] child_schedule = new long[2];
	boolean[] child_acks = new boolean[2];
	boolean[] child_schRcv = new boolean[2];

	private long initialTime;
	private final IRadioMonitor	radioMonitor;

	/**
	 * @param sender_init
	 * @param aggregator 
	 */
	public ScheduleMonitor(final GenericSender sender_init, AggregatorMonitor aggregator) {
		timerSender = new Timer();
		timerRadio = new Timer();
		hasSchedule = false;

		sender = sender_init;
		this.aggregator = aggregator;
		address = (short) (Spot.getInstance().getRadioPolicyManager()
				.getIEEEAddress() & 0xFFFFl);

		IRadioPolicyManager radio_policy_manager = Spot.getInstance()
				.getRadioPolicyManager();
		radio_policy_manager.setRxOn(true);

		children = ((MyRoutingMgr) (LowPan.getInstance().getRoutingManager())).myChildren;

		resetTime();
		radioMonitor = RadioMonitor.getInstance();
	}

	public void resetTime() {
		initialTime = System.currentTimeMillis();
	}

	public int indexOfChild(long childAddress){
		int index = -1;
		for (int i=0; i<children.size(); i++) {	
			if (((Address) children.elementAt(i)).address==childAddress){
				index = i;
			}
		}
		return index;
	}

	public short getAddress(){
		return address;
	}

	public long getScheduleTime(){
		return System.currentTimeMillis() - initialTime;
	}	

	private class SendMessage extends TimerTask {

		public SendMessage() {
			super();
		}

		public void run() {
			if (sendMessage(false)){
				// Schedule the next message, only if successful
				scheduleSendMessage();
			}
		}
	}

	public boolean sendMessage(boolean asAck){
		Vector pck = aggregator.getCache();
		System.out.println(getScheduleTime() + " - Forwarding packets from cache - " + pck.size());
		if (pck.size() == 0) {
			SendAckAsMessage();
			return true;
		} else {
			final ByteArrayOutputStream output_stream = Packet
					.packetsToByte(pck, address);

			final byte[] data = output_stream.toByteArray();
			final int length = output_stream.size();
			if (sender.sendData(data, length)) {
				aggregator.emptyCache();

				System.out.println(getScheduleTime() + " - Data sent, cache empty");
				if (asAck){
					scheduleDefinition();
				}
				return true;
			} else {
				// wait for resync
				resetSchedules();
				return false;
			}
		}
	}

	public boolean SendSchedule(long child, long child_schedule) {
		final ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
		final DataOutput            data_output   = new DataOutputStream(output_stream);

		short type = 1;
		short address = (short) (child - this.ADDRESS_PREFIX);

		try {
			data_output.writeShort(type);
			data_output.writeShort(address);
			data_output.writeLong(child_schedule);
		} catch (IOException e) {
			e.printStackTrace();
		}

		final byte[] data = output_stream.toByteArray();
		final int length = output_stream.size();
		if (sender.sendSchedule(data, length, new Address(child))){
			System.out.println(getScheduleTime() + " - Schedule sent " + 
					Integer.toHexString(address) + ": " + child_schedule);
			return true;
		}
		return false;
	}

	public void SendAckAsMessage() {
		final ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
		final DataOutput            data_output   = new DataOutputStream(output_stream);

		short type = 2;

		try {
			data_output.writeShort(type);
			data_output.writeShort(address);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void SendAck() {
		final ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
		final DataOutput            data_output   = new DataOutputStream(output_stream);

		short type = 2;

		try {
			data_output.writeShort(type);
			data_output.writeShort(address);
		} catch (IOException e) {
			e.printStackTrace();
		}

		final byte[] data = output_stream.toByteArray();
		final int length = output_stream.size();

		if (sender.sendData(data, length)){
			System.out.println(getScheduleTime() + " - Ack sent");
			scheduleDefinition();
		} else {
			scheduleSendAck();
		}
	}

	private class SendAck extends TimerTask {

		public SendAck() {
			super();
		}

		public void run() {
			if (hasSchedule && ! aggregator.cacheEmpty() ) {
				if (sendMessage(true))
					System.out.println(getScheduleTime() + " - Ack sent with message");
			} else { // normal acknowledge
				SendAck();
			}
		}
	}

	public void receivedAckBy(long address){
		receivedAckBy(address, false);
	}

	public void receivedAckBy(long address, boolean silent){
		int index = indexOfChild(address);
		if(index>=0){
			if (child_schRcv[index]){
				child_acks[index] = true;
				if (! silent) {
					System.out.println(getScheduleTime() + " - Ack received from " +
							Integer.toHexString((int)address));
				}
			}
		}


	}

	private class RadioTurnOn extends TimerTask {

		public RadioTurnOn() {
			super();
		}

		public void run() {
			for (int i = 0; i < child_acks.length; i++) {
				child_acks[i] = false;
			}

			IRadioPolicyManager radio_policy_manager = Spot.getInstance()
					.getRadioPolicyManager();
			radio_policy_manager.setRxOn(true);
			System.out.println(getScheduleTime() + " - Turned On");


			System.out.print(((MonitorApp)sender).getAbsoluteTime() + "A - ONt = " + (radioMonitor.getTotalRxOnTime()-initialDCOffset) + " - OFFt = " + radioMonitor.getTotalRxOffTime());
			System.out.println(" - DC = " + ( (radioMonitor.getTotalRxOnTime()-initialDCOffset ) 
					/ (radioMonitor.getTotalRxOnTime()-initialDCOffset + radioMonitor.getTotalRxOffTime())));
			// Schedule the next turn Off.
			scheduleNextRadioOff();
		}
	}

	private class RadioTurnOff extends TimerTask {

		public RadioTurnOff() {
			super();
		}

		public void run() {
			if (checkSchedule()){
				IRadioPolicyManager radio_policy_manager = Spot.getInstance()
						.getRadioPolicyManager();
				radio_policy_manager.setRxOn(false);
				System.out.println(getScheduleTime() + " - Turned Off");

				// Schedule the next turn On.
				scheduleNextRadioOn();
			}else{
				System.out.println(getScheduleTime() + " - Still waiting for acks!");
				scheduleNextRadioOff();
			}
			System.out.print(((MonitorApp)sender).getAbsoluteTime() + "A - ONt = " + (radioMonitor.getTotalRxOnTime()-initialDCOffset) + " - OFFt = " + radioMonitor.getTotalRxOffTime());
			System.out.println(" - DC = " + ( (radioMonitor.getTotalRxOnTime()-initialDCOffset ) 
					/ (radioMonitor.getTotalRxOnTime()-initialDCOffset + radioMonitor.getTotalRxOffTime())));
		}		
	}

	public class DefineSchedule extends TimerTask {

		public DefineSchedule() {
			super();
		}

		public void run() {
			if (hasSchedule){
				timerRadio.cancel();
				timerRadio = new Timer();
			}

			if (children.size()==0){
				scheduleNextRadioOff();
			}else{
				startingTime = System.currentTimeMillis();
				for (int i = 0; i < children.size(); i++) {	
					System.out.println(getScheduleTime() + " - Schedule for "+((Address)children.elementAt(i)).getID());
					child_schedule[i] = (listening_time * i) / (children.size() + 1) + 500;
					child_acks[i] = false;
					child_schRcv[i] = SendSchedule(((Address) children.elementAt(i)).address, child_schedule[i]);
				}
				// Schedule the first radio on.
				scheduleNextRadioOn(0);
			}
			hasSchedule = true;
		}
	}

	public boolean checkSchedule(){
		boolean ok = true;
		if (children.size()>0){
			int k = 0;
			for (int i = 0; i < children.size(); i++) {	
				if (! child_acks[i]){
					long middleTime = System.currentTimeMillis();
					long additionalTime = (30*1000) - (middleTime - startingTime) % (30*1000);
					child_schedule[i] = additionalTime + (listening_time * k) / (children.size() + 1) + 500;
					child_schRcv[i] = SendSchedule(((Address) children.elementAt(i)).address, child_schedule[i]);
					k++;
					ok = false;
				}
			}
		}
		return ok;
	}

	/**
	 * connection_send
	 */
	public void stop() {
		timerRadio.cancel();
		timerSender.cancel();
	}

	public void resetSchedules() {
		IRadioPolicyManager radio_policy_manager = Spot.getInstance()
				.getRadioPolicyManager();
		radio_policy_manager.setRxOn(true);
		System.out.println(getScheduleTime() + " - Turned On");

		timerRadio.cancel();
		timerRadio = new Timer();
		timerSender.cancel();
		timerSender = new Timer();
	}

	private void scheduleSendMessage() {
		scheduleSendMessage(SEND_INTERVAL);
	}

	public void scheduleSendMessage(long duration) {
		timerSender.schedule(new SendMessage(), duration);
	}

	public void scheduleDefinition() {
		if (! hasSchedule){
			initialDCOffset = radioMonitor.getTotalRxOnTime();
			System.out.print(((MonitorApp)sender).getAbsoluteTime() + "A - Initial - ONt = " + initialDCOffset + " - OFFt = " + radioMonitor.getTotalRxOffTime());
			System.out.println(" - DC = " + ((initialDCOffset )/ (initialDCOffset + radioMonitor.getTotalRxOffTime())));
		}
		timerSender.schedule(new DefineSchedule(), listening_time / 2);
	}

	public void scheduleSendAck() {
		final long duration = 200;
		timerSender.schedule(new SendAck(), duration);
	}

	private void scheduleNextRadioOn() {
		long interval = (SEND_INTERVAL) - 
				(System.currentTimeMillis() - startingTime) % (SEND_INTERVAL);
		System.out.println(getScheduleTime() + " - Next on after: " + interval);
		timerRadio.schedule(new RadioTurnOn(), interval);
	}
	private void scheduleNextRadioOn(long interval) {
		timerRadio.schedule(new RadioTurnOn(), interval);
	}

	private void scheduleNextRadioOff() {
		timerRadio.schedule(new RadioTurnOff(), listening_time);
	}
}