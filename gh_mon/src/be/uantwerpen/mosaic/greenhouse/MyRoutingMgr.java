package be.uantwerpen.mosaic.greenhouse;

import java.util.Vector;

import com.sun.spot.peripheral.NoRouteException;
import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.radio.ILowPan;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.peripheral.radio.mhrp.interfaces.IMHEventListener;
import com.sun.spot.peripheral.radio.routing.RouteInfo;
import com.sun.spot.peripheral.radio.routing.RouteTable;
import com.sun.spot.peripheral.radio.routing.interfaces.IRoutingManager;
import com.sun.spot.peripheral.radio.routing.interfaces.RouteEventClient;

public class MyRoutingMgr implements IRoutingManager {

	long[] spots = {0x00144F01000015BDl, // sink 0
			0x00144F0100005812l, // 1
			0x00144F0100001193l, // 2
			0x00144F0100001A57l, // 3
			0x00144F0100001579l, // 4
			0x00144F01000016D7l, // 5
			0x00144F01000012B5l, // 6
			0x00144F0100001154l, // 7
			0x00144F0100001519l};// 8
	int[][][] routingTable = {
			// node 0 goes to... node X with hop Y
			{{0,0}, {1,1}, {2,1}, {1,2}, {2,2}, {2,2}, {1,3}, {1,3}, {1,4}},
			// node 1 goes to... node X with hop Y
			{{0,1}, {1,0}, {0,2}, {3,1}, {0,3}, {0,3}, {3,2}, {3,2}, {3,3}},
			// node 2 goes to... node X with hop Y
			{{0,1}, {0,2}, {2,0}, {0,3}, {4,1}, {5,1}, {0,4}, {0,4}, {0,5}},
			// node 3 goes to... node X with hop Y
			{{1,2}, {1,1}, {1,3}, {3,0}, {1,4}, {1,4}, {6,1}, {7,1}, {7,2}},
			// node 4 goes to... node X with hop Y
			{{2,2}, {2,3}, {2,1}, {2,4}, {4,0}, {2,2}, {2,5}, {2,5}, {2,6}},
			// node 5 goes to... node X with hop Y
			{{2,2}, {2,3}, {2,1}, {2,4}, {2,2}, {5,0}, {2,5}, {2,5}, {2,6}},
			// node 6 goes to... node X with hop Y
			{{3,3}, {3,2}, {3,4}, {3,1}, {3,5}, {3,5}, {6,0}, {3,2}, {3,3}},
			// node 7 goes to... node X with hop Y
			{{3,3}, {3,2}, {3,4}, {3,1}, {3,5}, {3,5}, {3,2}, {7,0}, {8,1}},
			// node 8 goes to... node X with hop Y
			{{7,4}, {7,3}, {7,5}, {7,2}, {7,6}, {7,6}, {7,3}, {7,1}, {8,0}},
	};
	IRadioPolicyManager rpm;

	private final long my_addr;
	private int my_addr_index = 0;
	private final long parent;
	public String parent_addr;
	public Vector myChildren;

	public MyRoutingMgr(){
		rpm = Spot.getInstance().getRadioPolicyManager();

		my_addr = rpm.getIEEEAddress();
		for (int i=0; i<spots.length; i++){
			if (spots[i]==my_addr)
				my_addr_index = i;
		}

		parent = getRouteInfo(spots[0]).nextHop;
		parent_addr = new Address(parent).toString();

		myChildren = new Vector();
		for (int i=my_addr_index+1; i<spots.length; i++){
			if (routingTable[my_addr_index][i][1] == 1)
				myChildren.addElement((Object)new Address(spots[i]));
		}
	}

	public RouteInfo getRouteInfo(long arg0) {
		int my_dest_index = 0;

		for (int i=0; i<spots.length; i++){
			if (spots[i]==arg0)
				my_dest_index = i;
		}

		int[] dest_hop = routingTable[my_addr_index][my_dest_index];
		/*
		System.out.println("MY: "+my_addr_index+ "("+
				Integer.toHexString((int)spots[my_addr_index])+") - Dest: " + 
				my_dest_index + "("+Integer.toHexString((int)spots[my_dest_index])
				+") --> " + dest_hop[0]+" - " + dest_hop[1]);
		*/
		return new RouteInfo(arg0, spots[dest_hop[0]], dest_hop[1]);
	}

	public boolean getEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getServiceName() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getStatus() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isRunning() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean pause() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean resume() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setEnabled(boolean arg0) {
		// TODO Auto-generated method stub

	}

	public void setServiceName(String arg0) {
		// TODO Auto-generated method stub @Override

	}

	public boolean start() {
		return true;
	}

	public boolean stop() {
		return true;
	}

	public void addTag(String arg0) {
		// TODO Auto-generated method stub

	}

	public String getTagValue(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public String[] getTags() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean hasTag(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public void removeTag(String arg0) {
		// TODO Auto-generated method stub

	}

	public void addEventListener(IMHEventListener arg0) {
		// TODO Auto-generated method stub

	}

	public void deregisterEventListener(IMHEventListener arg0) {
		// TODO Auto-generated method stub

	}

	public boolean findRoute(long arg0, RouteEventClient arg1, Object arg2)
			throws NoRouteException {
		// TODO Auto-generated method stub
		return false;
	}

	public int getMaximumHops() {
		// TODO Auto-generated method stub
		return 0;
	}

	public RouteTable getRoutingTable() {
		// TODO Auto-generated method stub1
		return null;
	}

	public void initialize(long arg0, ILowPan arg1) {
		// TODO Auto-generated method stub

	}

	public boolean invalidateRoute(long arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	public void registerEventListener(IMHEventListener arg0) {
		// TODO Auto-generated method stub

	}

	public void removeEventListener(IMHEventListener arg0) {
		// TODO Auto-generated method stub

	}

}